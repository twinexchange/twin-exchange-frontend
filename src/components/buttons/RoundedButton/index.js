import React from 'react'
import {RoundedButtonContainer, RoundedButtonMainTitle} from './styled'

export const RoundedButton = ({title, backgroundColor, hoverBackgroundColor, onClick}) => {
    return(
        <RoundedButtonContainer backgroundColor={backgroundColor} hoverBackgroundColor={hoverBackgroundColor} onClick={onClick} >
            <RoundedButtonMainTitle boldText={true}>
                {title}
            </RoundedButtonMainTitle>
        </RoundedButtonContainer>
    )
}