import styled from 'styled-components'

export const RoundedButtonContainer = styled.div`
  width: 150px;
  height: max-content;
  background-color: ${props => props.backgroundColor || "transparent"};
  border-radius: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 5px 5px 15px 5px rgba(0,0,0,0.10);
  transition: background-color 0.25s ease-in-out;
  cursor: pointer;

  &:hover {
    background-color: ${props => props.hoverBackgroundColor || "transparent"};
  }
`;

export const RoundedButtonMainTitle = styled.p`
  font-size: 15px;
  margin: 8px;
  color: ${props => props.textColor || "#ffffff"};
  font-weight: ${props => props.boldText === true ? 'bold' : 'normal'};
`;