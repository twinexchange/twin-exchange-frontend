import React from 'react'
import {ButtonContainer, ButtonContent, ButtonText, Icon} from './styled'

export const IconRoundedButton = ({text, backgroundColor, textColor, logo, onClick, type}) => {
    return(
        <ButtonContainer backgroundColor={backgroundColor} onClick={onClick} type={type} >
           <ButtonContent centered={logo ? false : true}>
            <ButtonText textColor={textColor} boldText={true}>
                    {text}
                </ButtonText>
                {
                    logo && <Icon src={logo} alt="icon" />
                }
           </ButtonContent>
        </ButtonContainer>
    )
}