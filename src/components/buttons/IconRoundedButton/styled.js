import styled from 'styled-components'

export const ButtonContainer = styled.button`
  width: 18vw;
  height: 40px;
  border: 0px;
  border-radius: 10px;
  text-align: center;
  cursor: pointer;
  background: ${props => props.backgroundColor ? props.backgroundColor : "#FFFFFF"};
  -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
  box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
  display: flex;
  justify-content: center;
  margin: 10px 0px;
  align-items: center;
`;

export const ButtonContent = styled.div`
  padding: ${props => props.centered ? '0px' : '0px 0px 0px 10%'};
  display: flex;
  width: 100%;
  justify-content: space-around;
  align-items: center;
`

export const ButtonText = styled.p`
  font-size: 15px;
  margin: 8px;
  color: ${props => props.textColor ? props.textColor : "#ffffff"};
  font-weight: ${props => props.boldText === true ? 'bold' : 'normal'};
`;

export const Icon = styled.img`
  height: 100%;
  object-fit: none;
`