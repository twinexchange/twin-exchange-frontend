import React from 'react'
import {Container, OptionsContainer, ArrowSignal} from './styled'
import {SelectOption} from './SelectOption'
import arrow from '../../images/certificacion_arrow.svg'
import {LanguageContext as LanguageList} from '../../utils/languageSelect'
import {OptionsContext} from '../../Context'

export const LanguageSelector = ({down}) => {
    return(
        <Container behind={down}>
            <OptionsContext.Consumer>
                {
                    ({setLanguage}) => (
                        <OptionsContainer>
                            <SelectOption item={LanguageList.spanish} onClick={() => setLanguage(LanguageList.spanish)} />
                            <SelectOption item={LanguageList.france} onClick={() => setLanguage(LanguageList.france)} />
                            <SelectOption item={LanguageList.indian} onClick={() => setLanguage(LanguageList.indian)} />
                            <SelectOption item={LanguageList.english} onClick={() => setLanguage(LanguageList.english)} />
                            <SelectOption item={LanguageList.italian} onClick={() => setLanguage(LanguageList.italian)} />
                            <SelectOption item={LanguageList.japanese} onClick={() => setLanguage(LanguageList.japanese)} />
                        </OptionsContainer>
                    )
                }
            </OptionsContext.Consumer>
            <ArrowSignal src={arrow} />
        </Container>
    )
}