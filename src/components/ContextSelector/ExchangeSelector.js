import React from 'react'
import {Container, OptionsContainer, ArrowSignal} from './styled'
import {SelectOption} from './SelectOption'
import arrow from '../../images/certificacion_arrow.svg'
import {ExchangeContext} from '../../utils/languageSelect'
import {ExchangeMoneyContext} from '../../Context'

export const ExchangeSelector= ({down}) => {
    return(
        <Container behind={down}>
            <ExchangeMoneyContext.Consumer>
                {
                    ({setExchange}) => (
                        <OptionsContainer>
                            <SelectOption item={ExchangeContext.pesos} onClick={() => setExchange(ExchangeContext.pesos)} />
                            <SelectOption item={ExchangeContext.francs} onClick={() => setExchange(ExchangeContext.francs)} />
                            <SelectOption item={ExchangeContext.dollars} onClick={() => setExchange(ExchangeContext.dollars)} />
                            <SelectOption item={ExchangeContext.dollarsUsa} onClick={() => setExchange(ExchangeContext.dollarsUsa)} />
                            <SelectOption item={ExchangeContext.euros} onClick={() => setExchange(ExchangeContext.euros)} />
                            <SelectOption item={ExchangeContext.yens} onClick={() => setExchange(ExchangeContext.yens)} />
                        </OptionsContainer>
                    )
                }
            </ExchangeMoneyContext.Consumer>
            <ArrowSignal src={arrow} />
        </Container>
    )
}