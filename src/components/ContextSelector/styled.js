import styled, {keyframes} from 'styled-components'
const fadeIn = keyframes`
  from {
    opacity: 0
  }
  to {
    opacity: 1
  }
`;

export const Container = styled.div`
    background: #FFFFFF;
    position: absolute;
    transform: ${props => props.behind === true ? 'translateY(65%)' : 'translateY(-65%)'};
    display: flex;
    justify-content: center;
    padding: 10px 20px;
    border-radius: 15px;
    box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.25);
    -webkit-box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.05);
    -moz-box-shadow: 2px 4px 48px 0px rgba(0,0,0,0.05);
    flex-direction: column;
    align-items: center;
    animation: ${fadeIn} 0.25s linear;

    &::before{
        content: '';
        height: 0;
        width: 0;
        display: ${props => props.behind === true ? 'none' : 'inline-block'};
        position: absolute;
        bottom: -15px;
        border-style: solid;
        border-width: 15px 7.5px 0 7.5px;
        border-color: #FFFFFF transparent transparent transparent;
    }

    &::after{
        content: '';
        height: 0;
        width: 0;
        display: inline-block;
        position: absolute;
        top: -15px;
        border-style: solid;
        border-width: 0 7.5px 15px 7.5px;
        border-color: transparent transparent #FFFFFF transparent;
    }
`

export const LanguageSelect = styled.div`
    display: flex;
    align-items: center;
    background: #FFFFFF;
    padding: 10px 15px;
    margin: ${props => props.selected ? "10px 10px" : "0px"};
    border-radius: 10px;
    box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.05);
    -webkit-box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.05);
    -moz-box-shadow: 2px 4px 48px 0px rgba(0,0,0,0.05);
    cursor: pointer;
    color: #575F6B;
    animation: ${fadeIn} 0.25s linear;

    &:hover{
        background: #3395FF;
        transition-duration: 0.5s;
        font-weight: bold;
        color: #FFFFFF !important;
        box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.2);
        -webkit-box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.2);
        -moz-box-shadow: 2px 4px 48px 0px rgba(0,0,0,0.2);
    }
`

export const OptionsContainer = styled.div`
    display: flex;
    flex-direction: column;
    max-height: 250px;
    overflow: scroll;

    &::-webkit-scrollbar{
        display: none;
    }
`

export const FlagImage = styled.img`
    margin-right: 10px;

`

export const LanguageText = styled.span`
    font-size: 15px;
`

export const MoneyText = styled.span`
    margin-left: 15px;
`

export const ArrowSignal = styled.img`

`
