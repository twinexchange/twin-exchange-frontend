import React from 'react'
import {LanguageSelect, FlagImage, LanguageText, MoneyText} from './styled'

export const SelectOption = ({item, onClick, selected}) => {
    return(
        <LanguageSelect onClick={onClick}>
            <FlagImage src={item.image} />
            <LanguageText>{item.name}</LanguageText>
            {
                item.money && <MoneyText>{item.money}</MoneyText>
            }
        </LanguageSelect>
    )
}