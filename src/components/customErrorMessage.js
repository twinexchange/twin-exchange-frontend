import styled from 'styled-components'

export const customErrorMessage = styled.div`
    color: #FF3333;
    font-size: 14px;
    margin-left: 5%;
    font-weight: bold;
`

export const ErrorMessageText = styled.p`
    color: #FF3333;
    font-size: 14px;
    text-align: center;
    font-weight: bold;
`