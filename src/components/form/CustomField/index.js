import React from 'react'
import { FieldContainer, FormikField, FormikBellowLabel } from './styled'

//Warning: Use this only in a form with Formik
export const CustomField = ({type, id, className, name, placeholder, textBellow}) => {

    return(
        <FieldContainer>
            <FormikField 
                type={type}
                id={id}
                className={className}
                name={name}
                placeholder={placeholder}
            />
           {
               textBellow && <FormikBellowLabel>{textBellow}</FormikBellowLabel>
           }
        </FieldContainer>
    )
}