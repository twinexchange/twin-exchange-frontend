import styled from 'styled-components'
import { Field } from "formik";

export const FieldContainer = styled.div`
    margin: 10px 0px;
    display: flex;
    flex-direction: column;
`

export const FormikField = styled(Field)`
    width: 18vw;
    height: 40px;
    border: 0px;
    border-radius: 10px;
    -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
    box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
    text-align: center;

    &::placeholder{
        color: #3395FF;
        text-align: initial;
        font-weight: bold;
        padding: 0px 10%;
    }
`;

export const FormikBellowLabel = styled.label`
    color: #3395FF;
    font-size: 12px;
    padding: 0px 10%;
`