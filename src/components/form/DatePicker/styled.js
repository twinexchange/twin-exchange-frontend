import styled from 'styled-components'

export const FormikField = styled.input`
    width: 18vw;
    height: 40px;
    border: 0px;
    border-radius: 10px;
    -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
    box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
    text-align: center;
    background: #ffffff;
    color: #3395FF !important;
    font-weight: bold;

    &::placeholder{
        color: #3395FF !important;
        text-align: initial;
        font-weight: bold;
        padding: 0px 10%;
    }
`;