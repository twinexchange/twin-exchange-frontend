import React from "react";
import { useField, useFormikContext } from "formik";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {FormikField} from './styled'
import * as moment from 'moment'

export const DatePickerField = ({ ...props }) => {
  const { setFieldValue } = useFormikContext();
  const [field] = useField(props);
  return (
    <DatePicker
      {...field}
      {...props}
      selected={(field.value && new Date(field.value)) || null}
      onChange={val => {
        console.log(moment(val).format("DD/MM/YYYY"))
        setFieldValue(field.name,val);
      }}
      customInput={<FormikField />}
      placeholderText={props.placeholder}
      dateFormat="dd/MM/yyyy"
    />
  );
};

export default DatePickerField;
