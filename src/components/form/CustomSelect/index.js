import React from 'react'
import { Container, FormikSelect } from './styled'

//Warning: Use this only in a form with Formik
export const CustomSelect = ({type, id, className, name, placeholder, children, value, onChange}) => {
    return(
        <Container>
            <FormikSelect
                type={type}
                id={id}
                className={className}
                name={name}
                placeholder={placeholder}
                as="select"
                value={value}
                onChange={onChange}
            >
                {children}
            </FormikSelect>
        </Container>
    )
}