import React, { useState } from 'react'
import {
    Container,
    CustomSelect,
    OptionContainer,
    OptionTextContainer,
    OptionText,
    CoinIcon,
} from './styled'
import twinCoin from '../../../images/icons/crypto/Twinncoin.svg'

const SelectDropdown = () => {
    const options = [
        { value: 'vanilla', 
          label: <OptionContainer>
                    <CoinIcon src={twinCoin} />
                    <OptionTextContainer>
                        <OptionText>Twincoin</OptionText>
                        <OptionText>Saldo disponible: 0.00</OptionText>
                    </OptionTextContainer>
                 </OptionContainer> 
        },
        { value: 'vanilla', 
          label: <OptionContainer>
                    <CoinIcon src={twinCoin} />
                    <OptionTextContainer>
                        <OptionText>Twincoin2</OptionText>
                        <OptionText>Saldo disponible: 0.00</OptionText>
                    </OptionTextContainer>
                 </OptionContainer> 
        },
      ];
    
    const [option, setOption] = useState(options[0])
    
    const customStyles = {
        control: base => ({
            ...base,
            height: 50,
            display: 'flex',
            border: 'none',
            borderRadius: '15px',
            boxShadow: '5px 5px 5px 2px rgba(0,0,0,0.05)'
        }),
        container: base => ({
            ...base,
            border: 'none',
            width: '100%',
        }),
        singleValue: base => ({
            ...base,
            height: 80,
        }),
        option: ((styles, {isFocused}) => {
            return {
                ...styles,
                backgroundColor: isFocused ? '#bcdfeb' : 'null',
            }
        }),
        dropdownIndicator: base => ({
            ...base,
            boxShadow: '5px 5px 6px 2px rgba(0,0,0,0.1)',
            marginRight: '20px',
            color: '#99A0AF',
            borderRadius: '8px',
        }),
        indicatorSeparator: (base) => ({display:'none'})
    };


    return(
        <Container>
            <CustomSelect
                value={option}
                onChange={(selected) => setOption(selected)}
                options={options}
                styles={customStyles}
                isSearchable={false}
            />
        </Container>
    )
}

export default SelectDropdown