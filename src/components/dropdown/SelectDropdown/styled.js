import styled from 'styled-components'
import Select from 'react-select';

export const Container = styled.section`
    display: flex;
    flex-direction: column;
    
`

export const CustomSelect = styled(Select)`
`

export const OptionContainer = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    height: 100%;
`

export const OptionTextContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const OptionText = styled.p`
    color: #99A0AF;
    font-size: 10px;
    margin: 0px;
    width: 250px;
`

export const CoinIcon = styled.img`
    height: 20px;
    width: 20px;
    margin: 0px 10px;
`