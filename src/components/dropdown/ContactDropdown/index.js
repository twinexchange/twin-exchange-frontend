import React, { useState } from 'react'
import {
    Container,
    CustomSelect,
    OptionContainer,
    OptionText,
    TwinIcon,
    Exchange,
    InviteButton,
    SelectedTitle,
    SelectedIcon
} from './styled'
import Select, {components} from 'react-select';
import person from '../../../images/icons/person.svg'
import exchange from '../../../images/icons/twin_exchange.svg'
import twinIcon from '../../../images/icons/logo twin.svg'
import styled from 'styled-components';

const ContactDropdown = () => {
    const options = [
        { value: 'David Manuel López Castro', 
          label: <OptionContainer>
                    <Exchange src={exchange} />
                    <TwinIcon src={twinIcon} />
                    <OptionText>David Manuel López Castro</OptionText>
                    <InviteButton onClick={() => console.log("click")}>
                        Invitar
                    </InviteButton>
                 </OptionContainer> 
        },
        { value: 'Edgar Javier Gómez Esguerra', 
          label: <OptionContainer>
                    <Exchange src={exchange} />
                    <TwinIcon src={twinIcon} />
                    <OptionText>Edgar Javier Gómez Esguerra</OptionText>
                    <InviteButton onClick={() => console.log("click")}>
                        Invitar
                    </InviteButton>
                 </OptionContainer> 
        },
        { value: 'Rodrigo Alberto Gonzalez del Roble', 
          label: <OptionContainer>
                    <Exchange src={exchange} />
                    <TwinIcon src={twinIcon} />
                    <OptionText>Rodrigo Alberto Gonzalez del Roble</OptionText>
                    <InviteButton onClick={() => console.log("click")}>
                        Invitar
                    </InviteButton>
                 </OptionContainer> 
        },
      ];
    
    const [optionName, setOption] = useState(options[0])

    const changeValue = async (selected) => {
        setOption(selected); 
    }
    
    const customStyles = {
        control: base => ({
            ...base,
            height: 50,
            display: 'flex',
            border: 'none',
            borderRadius: '15px',
            boxShadow: '5px 5px 5px 2px rgba(0,0,0,0.05)',
            zIndex: 2,
        }),
        container: base => ({
            ...base,
            border: 'none',
            width: '100%',
        }),
        singleValue: base => ({
            ...base,
            height: 80,
            display: 'flex',
            alignContent: 'flex-start',
            width: '100%',
            zIndex: 99
        }),
        valueContainer: base => ({
            ...base,
            display: 'flex',
            margin: 'auto 0px',
        }),
        dropdownIndicator: base => ({
            ...base,
            boxShadow: '5px 5px 6px 2px rgba(0,0,0,0.1)',
            zIndex: 1,
            marginRight: '20px',
            color: '#99A0AF',
            borderRadius: '8px',
        }),
        option: ((styles, {isFocused}) => {
            return {
                ...styles,
                backgroundColor: isFocused ? '#bcdfeb' : 'null',
            }
        }),
        menu: base => ({
            ...base,
            marginTop: '-30px',
            zIndex: 1,
            background: '#F8F8FC',
            border: 'none',
            boxShadow: '5px 5px 6px 2px rgba(0,0,0,0.1)',
            paddingTop: '30px',
            paddingLeft: '20px',
            paddingRight: '20px'
        }),
        indicatorSeparator: (base) => ({display:'none'}),
    };

    const SelectedOption = props => {
        return (
          <components.SingleValue {...props}>        
            <OptionContainer>
                <SelectedIcon src={person} />
                <SelectedTitle>{props.data.value}</SelectedTitle>
            </OptionContainer>
          </components.SingleValue>
        );
    };

    return(
        <Container>
            <CustomSelect
                value={optionName}
                onChange={(selected) => changeValue(selected)}
                options={options}
                styles={customStyles}
                isSearchable={false}
                components={{ SingleValue: SelectedOption,  DropdownIndicator: () => null }}
            />
        </Container>
    )
}


export default ContactDropdown