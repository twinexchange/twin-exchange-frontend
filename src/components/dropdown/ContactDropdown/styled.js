import styled from 'styled-components'
import Select from 'react-select';

export const Container = styled.section`
    display: flex;
    flex-direction: column;
`

export const CustomSelect = styled(Select)`
`

export const OptionContainer = styled.div`
    display: flex;
    height: 40px;
    align-items: center;
    margin: auto 0px;
    width: 100%;
    border-bottom: 0.5px solid rgba(112, 112, 112, 0.2);
`

export const OptionText = styled.p`
    color: #81878F;
    font-size: 16px;
    display: flex;
    align-items: center;
    margin-left: 5px;
`

export const TwinIcon = styled.img`
    height: 20px;
    width: 20px;
    margin: 0px 5px;
`

export const Exchange = styled(TwinIcon)`

`

export const InviteButton = styled.div`
    font-weight: bold;
    margin-left: auto;
    background: #3395FF;
    font-size: 14px;
    padding: 5px 25px;
    border-radius: 5px;
    cursor: pointer;
    color: #ffffff;
    margin-right: 5%;
`

export const SelectedTitle = styled(OptionText)`

`

export const SelectedIcon = styled(TwinIcon)`

`