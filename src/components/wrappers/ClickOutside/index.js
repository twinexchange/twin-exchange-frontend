import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import {Wrapper} from './styled'

function useOutsideClick(ref, clickFunction) {
  useEffect(() => {
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        clickFunction()
      }
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
}

function ClickOutside(props) {
  const wrapperRef = useRef(null);
  useOutsideClick(wrapperRef, props.clickFunction);

  return <Wrapper className="outside" ref={wrapperRef}>{props.children}</Wrapper>;
}

ClickOutside.propTypes = {
  children: PropTypes.element.isRequired
};

export default ClickOutside;