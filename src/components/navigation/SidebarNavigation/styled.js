import styled from 'styled-components'
import {Link} from 'react-router-dom'

export const Container = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
`;

export const OptionContainer = styled(Link)`
    display: flex;
    width: 70%;
    align-items: center;
    margin: 12px 0px;
    cursor: pointer;
    text-decoration: none;
`;

export const Button = styled.img`
    padding: 8px;
    width: 20px;
    height: 20px;
    -webkit-box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.1); 
    box-shadow: 3px 3px 17px 0px rgba(0,0,0,0.1);
    border-radius: 10px;
    object-fit: contain;
    margin-right: 20px;
`;

export const Title = styled.div`
    font-size: 10px;
    color: #99A0AF;
    font-weight: bold;
`