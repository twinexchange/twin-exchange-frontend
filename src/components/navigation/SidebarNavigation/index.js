import React from 'react'
import { 
    Container,
    OptionContainer,
    Button,
    Title
} from './styled'
import sidebarImage from '../../../images/sidebar/dashboard.svg'
import directoryBussiness from '../../../images/sidebar/directory_business.svg'
import twinBussiness from '../../../images/sidebar/twin_bussiness.svg'
import movements from '../../../images/sidebar/movements.svg'
import comissions from '../../../images/sidebar/comissions.svg'
import riskAdvices from '../../../images/sidebar/risk_advices.svg'
import legalAdvices from '../../../images/sidebar/legal_advices.svg'
import about from '../../../images/sidebar/about.svg'
import logout from '../../../images/sidebar/logout.svg'
import { useHistory } from "react-router-dom";

const SideBarNavigation = ({logoutFunction}) => {
    let history = useHistory();

    return (
        <Container>
            <OptionContainer to={'/app/twincoin'}>
                <Button src={sidebarImage} />
                <Title>DASHBOARD</Title>
            </OptionContainer>
            <OptionContainer to={'/directory-business'}>
                <Button src={directoryBussiness} />
                <Title>DIRECTORIO EMPRESARIAL</Title>
            </OptionContainer>
            <OptionContainer to={'/'}>
                <Button src={twinBussiness} />
                <Title>TWINBUSINESS</Title>
            </OptionContainer>
            <OptionContainer to={'/'}>
                <Button src={movements} />
                <Title>MOVIMIENTOS</Title>
            </OptionContainer>
            <OptionContainer to={'/'}>
                <Button src={comissions} />
                <Title>COMISIONES</Title>
            </OptionContainer>
            <OptionContainer to={'/'}>
                <Button src={riskAdvices} />
                <Title>AVISOS DE RIESGO</Title>
            </OptionContainer>
            <OptionContainer to={'/'}>
                <Button src={legalAdvices} />
                <Title>AVISOS LEGALES</Title>
            </OptionContainer>
            <OptionContainer to={'/'}>
                <Button src={about} />
                <Title>¿QUIÉNES SOMOS?</Title>
            </OptionContainer>
            <OptionContainer onClick={logoutFunction}>
                <Button src={logout} />
                <Title>CERRAR SESIÓN</Title>
            </OptionContainer>
        </Container>
    )
}

export default SideBarNavigation;