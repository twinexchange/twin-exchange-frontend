import styled from 'styled-components'

export const Container = styled.div`
    margin-bottom: 5%;
`

export const Title = styled.p`
    color: #99A0AF;
    font-size: 35px;
    font-weight: lighter;
    margin: 0px
`

export const Subtitle = styled.p`
    color: #3395FF;
    margin: 0px;
`