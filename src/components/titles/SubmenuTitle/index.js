import React from 'react'
import {
    Container, 
    Title, 
    Subtitle,
} from './styled'


const SubmenuTitle = ({name, title, subtitle, textBehind}) =>(
    <Container>
        <Title>{title} {name && name.toUpperCase()}</Title>
        <Subtitle>{subtitle}</Subtitle>
        {
            textBehind && <Subtitle>{textBehind}</Subtitle>
        }
    </Container>
)

export default SubmenuTitle