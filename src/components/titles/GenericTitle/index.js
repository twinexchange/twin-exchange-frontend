import React from 'react'
import {
    Container, 
    Title, 
} from './styled'


const GenericTitle = ({ title }) =>(
    <Container>
        <Title>{title}</Title>
    </Container>
)

export default GenericTitle