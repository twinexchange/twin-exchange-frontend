import styled from 'styled-components'

export const Container = styled.div`
`

export const Title = styled.p`
    color: #3395FC;
    font-weight: normal;
    margin: 15px 0px 10px 0px;
    font-size: 15px;
`
