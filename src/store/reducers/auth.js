import typeToReducer from 'type-to-reducer'
import { LOGIN, REGISTER } from '../actions/auth/types';

const initialState = {
    login: {
        isFetching: false,
        loginError: false,
        loginSuccess: false,
    },
    register: {
        isFetching: false,
        loginError: false,
        loginSuccess: false,
    }
}

export const authReducer = typeToReducer({
    [LOGIN]: {
        PENDING: () => ({
            ...initialState,
            login: {
                isFetching: true,
                loginError: false,
                loginSuccess: false,
            }
          }),
        ERROR: () => ({
            ...initialState,
            login: {
                isFetching: false,
                loginError: true,
                loginSuccess: false,
            }
          }),
        SUCCESS: () => ({
            ...initialState,
            login: {
                isFetching: false,
                loginError: false,
                loginSuccess: true,
            }
          }),
    },
    [REGISTER]: {
        PENDING: () => ({
            ...initialState,
            login: {
                isFetching: true,
                loginError: false,
                loginSuccess: false,
            }
          }),
        ERROR: () => ({
            ...initialState,
            login: {
                isFetching: false,
                loginError: true,
                loginSuccess: false,
            }
          }),
        SUCCESS: () => ({
            ...initialState,
            login: {
                isFetching: false,
                loginError: false,
                loginSuccess: true,
            }
          }),
    }
}, initialState)

export default authReducer