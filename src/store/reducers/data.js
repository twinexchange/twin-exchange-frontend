import typeToReducer from 'type-to-reducer'
import { SAVE_DATA } from '../actions/save/types';

const initialState = {
    userData: {}
}

export const dataReducer = typeToReducer({
    [SAVE_DATA]: (state, { payload }) => ({
        ...state,
        userData: {
            ...payload
        },
    })
}, initialState)

export default dataReducer