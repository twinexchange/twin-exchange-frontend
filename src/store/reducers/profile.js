import typeToReducer from 'type-to-reducer'
import { actionList } from '../../utils/dashboardList';
import { GET_DATA, SAVE_DATA } from '../actions/profile/types';

const initialState = {
    userData: {
        isFetching: false,
        loginError: false,
        loginSuccess: false,
    },
    
}

export const profileReducer = typeToReducer({
    [GET_DATA]: {
        PENDING: () => ({
            ...initialState,
            login: {
                isFetching: true,
                loginError: false,
                loginSuccess: false,
                
            }
          }),
        ERROR: () => ({
            ...initialState,
            userData: {
                isFetching: false,
                loginError: true,
                loginSuccess: false,
            }
          }),
        SUCCESS: () => ({
            ...initialState,
            userData: {
                isFetching: false,
                loginError: false,
                loginSuccess: true,
            }
          }),
    },
}, initialState)

export default profileReducer