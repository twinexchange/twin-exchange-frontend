import typeToReducer from 'type-to-reducer'
import { CREATE_WALLET, GET_WALLETS, DELETE_WALLET } from '../actions/wallet/types';

const initialState = {
    createWallet: {
        isFetching: false,
        createError: false,
        createSuccess: false,
    },
    getWallets: {
        isFetching: false,
        getError: false,
        getSuccess: false,
        wallets: [],
    },
    deleteWallet: {
        isFetching: false,
        deleteError: false,
        deleteSuccess: false,
    }
}

export const walletReducer = typeToReducer({
    [CREATE_WALLET]: {
        PENDING: () => ({
            ...initialState,
            createWallet: {
                isFetching: true,
                createError: false,
                createSuccess: false,
            }
          }),
        ERROR: () => ({
            ...initialState,
            createWallet: {
                isFetching: false,
                createError: true,
                createSuccess: false,
            }
          }),
        SUCCESS: () => ({
            ...initialState,
            createWallet: {
                isFetching: false,
                createError: false,
                createSuccess: true,
            }
          }),
    },
    [GET_WALLETS]: {
        PENDING: () => ({
            ...initialState,
            getWallets: {
                isFetching: true,
                createError: false,
                createSuccess: false,
            }
          }),
        ERROR: () => ({
            ...initialState,
            getWallets: {
                isFetching: false,
                createError: true,
                createSuccess: false,
            }
          }),
        SUCCESS: ( state, { payload }) => ({
            ...state,
            getWallets: {
                createSuccess: true,
                wallets: payload
            }
          }),
    },
    [DELETE_WALLET]: {
        PENDING: () => ({
            ...initialState,
            deleteWallet: {
                isFetching: true,
                deleteError: false,
                deleteSuccess: false,
            }
          }),
        ERROR: () => ({
            ...initialState,
            deleteWallet: {
                isFetching: false,
                deleteError: true,
                deleteSuccess: false,
            }
          }),
        SUCCESS: () => ({
            ...initialState,
            deleteWallet: {
                isFetching: false,
                deleteError: false,
                deleteSuccess: true,
            },
          }),
    },
}, initialState)

export default walletReducer