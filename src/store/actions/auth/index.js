import { createAction } from 'redux-actions';
import { LOGIN_ASYNC, REGISTER_ASYNC } from './types'

export const loginPending = createAction(LOGIN_ASYNC.PENDING);
export const loginError = createAction(LOGIN_ASYNC.ERROR)
export const loginSuccess= createAction(LOGIN_ASYNC.SUCCESS);

export const registerPending = createAction(REGISTER_ASYNC.PENDING);
export const registerError = createAction(REGISTER_ASYNC.ERROR)
export const registerSuccess= createAction(REGISTER_ASYNC.SUCCESS);