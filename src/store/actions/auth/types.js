import { createAsyncTypes } from '../../../utils/createTypes';

export const LOGIN = 'LOGIN';
export const LOGIN_ASYNC = createAsyncTypes('LOGIN');

export const REGISTER = 'REGISTER';
export const REGISTER_ASYNC = createAsyncTypes('REGISTER');