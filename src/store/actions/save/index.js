import { SAVE_DATA } from './types'
import { createAction } from 'redux-actions';

export const saveData = createAction(SAVE_DATA)