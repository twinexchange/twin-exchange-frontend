import { createAction } from 'redux-actions';
import { CREATE_WALLET_ASYNC, GET_WALLETS_ASYNC, DELETE_WALLET_ASYNC } from './types'

export const createWallet = createAction(CREATE_WALLET_ASYNC.PENDING);
export const createWalletError = createAction(CREATE_WALLET_ASYNC.ERROR)
export const createWalletSuccess= createAction(CREATE_WALLET_ASYNC.SUCCESS);

export const getWallets = createAction(GET_WALLETS_ASYNC.PENDING);
export const getWalletsError = createAction(GET_WALLETS_ASYNC.ERROR)
export const getWalletsSuccess= createAction(GET_WALLETS_ASYNC.SUCCESS);

export const deleteWallet = createAction(DELETE_WALLET_ASYNC.PENDING);
export const deleteWalletError = createAction(DELETE_WALLET_ASYNC.ERROR)
export const deleteWalletSuccess= createAction(DELETE_WALLET_ASYNC.SUCCESS);