import { createAsyncTypes } from '../../../utils/createTypes';

export const CREATE_WALLET = 'CREATE_WALLET';
export const CREATE_WALLET_ASYNC = createAsyncTypes('CREATE_WALLET');

export const GET_WALLETS = 'GET_WALLETS';
export const GET_WALLETS_ASYNC = createAsyncTypes('GET_WALLETS')

export const DELETE_WALLET = 'DELETE_WALLET';
export const DELETE_WALLET_ASYNC = createAsyncTypes('DELETE_WALLET')