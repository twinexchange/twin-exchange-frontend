import { createAction } from 'redux-actions';
import { GET_DATA_ASYNC, SAVE_DATA } from './types'

export const getProfilePending = createAction(GET_DATA_ASYNC.PENDING);
export const getProfilePendingError = createAction(GET_DATA_ASYNC.ERROR)
export const getProfilePendingSuccess= createAction(GET_DATA_ASYNC.SUCCESS);