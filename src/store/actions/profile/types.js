import { createAsyncTypes } from '../../../utils/createTypes';

export const GET_DATA = 'GET_DATA';
export const GET_DATA_ASYNC = createAsyncTypes('GET_DATA');