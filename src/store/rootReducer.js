import { combineReducers } from "redux";
import authReducer from "./reducers/auth";
import profileReducer from "./reducers/profile";
import dataReducer from "./reducers/data"
import walletReducer from "./reducers/wallet"

const rootReducer = combineReducers({
    dataReducer,
    authReducer,
    profileReducer,
    walletReducer,
});


export default rootReducer;
