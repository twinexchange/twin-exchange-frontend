import auth from '../../../api/auth';
import client from '../../../api/client';
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOGIN_ASYNC } from '../../actions/auth/types';
import {loginSuccess, loginError} from '../../actions/auth'

function* login({payload}) {
  try{
    const { email, password } = payload;
    const accessToken = yield call(auth.login, {email, password})

    console.log(accessToken)

    //Save token
    yield call(client.storeToken, accessToken.data.accessToken);
    yield put(loginSuccess());
  }catch(error){
    console.error(error)
    yield put(loginError())
  }
}

export default function* watchLogin() {
  yield takeLatest(LOGIN_ASYNC.PENDING, login);    
}