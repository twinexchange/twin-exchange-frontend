import { fork } from 'redux-saga/effects';
import loginSaga from './loginSaga';
import registerSaga from './registerSaga';

export default function* () {
  yield fork(loginSaga);
  yield fork(registerSaga);
}
