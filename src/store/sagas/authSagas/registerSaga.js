import auth from '../../../api/auth';
import client from '../../../api/client';
import { call, put, takeLatest } from 'redux-saga/effects';
import { REGISTER_ASYNC } from '../../actions/auth/types';
import {registerSuccess, registerError} from '../../actions/auth'
import * as moment from 'moment'

function* register({payload}) {
  try{
    console.log("register")
    const registerData = {
        ...payload, 
        address: "null",
        rfc: "null", 
        nip: "null", 
        identifier_uuid: 0, 
        role: 'USER', 
        digital_signature: "null",
        born_date: moment(payload.born_date).format("DD/MM/YYYY")
    };
    delete registerData.confirmPassword

    delete registerData.picked //TEMPORAL
    
    const accessToken = yield call(auth.register, registerData)

    console.log(accessToken)

    //Save token
    yield call(client.storeToken, accessToken.data.accessToken);
    yield put(registerSuccess());
  }catch(error){
    console.error(error)
    yield put(registerError())
  }
}

export default function* watchRegister() {
  yield takeLatest(REGISTER_ASYNC.PENDING, register);    
}