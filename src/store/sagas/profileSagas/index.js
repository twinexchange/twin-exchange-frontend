import { fork } from 'redux-saga/effects';
import getProfileDataSaga from './getProfileDataSaga';

export default function* () {
  yield fork(getProfileDataSaga);
}
