import auth from '../../../api/auth';
import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_DATA_ASYNC } from '../../actions/profile/types';
import {getProfilePendingSuccess, getProfilePendingError} from '../../actions/profile'
import {saveData} from '../../actions/save'

function* getProfileData() {
  try{
    const data = yield call(auth.get_data)

    yield put (saveData(data.data))
    yield put(getProfilePendingSuccess());
  }catch(error){
    console.error(error)
    yield put(getProfilePendingError())
  }
}

export default function* watchGetProfileData() {
  yield takeLatest(GET_DATA_ASYNC.PENDING, getProfileData);    
}