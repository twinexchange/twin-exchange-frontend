import wallet from '../../../api/wallet';
import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_WALLETS_ASYNC } from '../../actions/wallet/types';
import {getWalletsSuccess, getWalletsError} from '../../actions/wallet'

function* getWalletsSaga({payload}) {
    try {
        const { userId, coin } = payload;
        const walletList = yield call( wallet.getWallets, {user: userId}, coin.toLocaleLowerCase());
        yield put(getWalletsSuccess(walletList.data));
    } catch (error) {
        console.error(error)
        yield put(getWalletsError())
    }
}

export default function* watchGetWallets() {
  yield takeLatest(GET_WALLETS_ASYNC.PENDING, getWalletsSaga);    
}