import wallet from '../../../api/wallet';
import { call, put, takeLatest } from 'redux-saga/effects';
import { CREATE_WALLET_ASYNC } from '../../actions/wallet/types';
import {createWalletSuccess, createWalletError} from '../../actions/wallet'

function* createWalletSaga({payload}) {
    try {
        const { coin, name_wallet, user } = payload;
        const newWalletData = {
            name_wallet: name_wallet,
            user: user
        }
        const createWallet = yield call( wallet.createWallet, newWalletData, coin.toLocaleLowerCase());
        console.log(createWallet)
        yield put(createWalletSuccess());
    } catch (error) {
        console.error(error)
        yield put(createWalletError())
    }
}

export default function* watchCreateWallet() {
  yield takeLatest(CREATE_WALLET_ASYNC.PENDING, createWalletSaga);    
}