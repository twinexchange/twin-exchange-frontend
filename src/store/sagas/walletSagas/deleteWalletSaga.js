import wallet from '../../../api/wallet';
import { call, put, takeLatest } from 'redux-saga/effects';
import { DELETE_WALLET_ASYNC } from '../../actions/wallet/types';
import {deleteWalletError, deleteWalletSuccess} from '../../actions/wallet'

function* deleteWalletSaga({payload}) {
    try {
        const { userId, walletIndex ,walletId, coin } = payload;
        console.log(userId, walletIndex ,walletId, coin )
        const walletResponse = yield call( wallet.deleteWallet, {userId: userId, walletId: walletId}, coin.toLocaleLowerCase());
        console.log(walletResponse)
        yield put(deleteWalletSuccess(walletIndex));
    } catch (error) {
        console.error(error)
        yield put(deleteWalletError())
    }
}

export default function* watchGetWallets() {
  yield takeLatest(DELETE_WALLET_ASYNC.PENDING, deleteWalletSaga);    
}