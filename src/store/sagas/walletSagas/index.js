import { fork } from 'redux-saga/effects';
import createWalletSaga from './createWalletSaga';
import getWalletsSaga from './getWalletSaga'
import deleteWalletSaga from './deleteWalletSaga'

export default function* () {
  yield fork(createWalletSaga);
  yield fork(getWalletsSaga);
  yield fork(deleteWalletSaga);
}
