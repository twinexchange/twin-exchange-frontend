import { all } from "redux-saga/effects";
import authSagas from "./sagas/authSagas";
import profileSagas from "./sagas/profileSagas";
import walletSagas from "./sagas/walletSagas";

export default function* rootSaga() {
  yield all([authSagas(), profileSagas(), walletSagas()]);
}
