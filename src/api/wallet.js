import client from './client'

const ENDPOINT = "wallet"

const createWallet = (data, crytoWallet) => {
    const url = `${client.BASE_URL}/${crytoWallet}/${ENDPOINT}/create`; 
  //http://159.203.123.63/api/twincoin/wallet/create
    return  new Promise((resolve, reject) => {
      return client.post(url, data)
        .then(response => resolve(response))
        .catch(reject)
    });
};

const getWallets = (data, crytoWallet) => {
  const url = `${client.BASE_URL}/${crytoWallet}/${ENDPOINT}/get-wallets`; 
  return new Promise((resolve, reject) => {
    return client.post(url, data)
      .then(response => resolve(response))
      .catch(reject)
  });
};

const deleteWallet = (data, crytoWallet) => {
  const url = `${client.BASE_URL}/${crytoWallet}/${ENDPOINT}/delete-wallet`; 
  return new Promise((resolve, reject) => {
    return client.remove(url, data)
      .then(response => resolve(response))
      .catch(reject)
  });
};

export default {
    createWallet,
    getWallets,
    deleteWallet
}