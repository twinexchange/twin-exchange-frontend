import axios from "axios";

const BASE_URL = "http://159.203.123.63/api"

const TIMEOUT = 30000;

const client = axios.create({
    baseURL: BASE_URL,
    timeout: TIMEOUT,
    responseType: JSON,
    headers: {
        Accept: "application/json",
    }
})

const get = (...args) => {
  getToken()
  return new Promise((resolve, reject) => {
    client
      .get(...args)
      .then((response) => resolve(response))
      .catch(reject);
  });
};

const post = (...args) => {
    getToken()
    console.log(...args)
    return new Promise((resolve, reject) => {
      client
        .post(...args)
        .then((response) => resolve(response))
        .catch(reject);
    });
};

const remove = (url, data) => {
  getToken()
  console.log(url, data)
  return new Promise((resolve, reject) => {
    client.delete(url, {
      headers: {
        Authorization: client.defaults.headers.common["Authorization"]
      },
      data
    })
      .then((response) => resolve(response))
      .catch(reject);
  });
};

const getToken = () => {
  const token = localStorage.getItem("token")
  token ? client.defaults.headers.common["Authorization"] = `Bearer ${token}` : null
  //console.log(client.defaults.headers)
}

const addToken = (token) =>
  (client.defaults.headers.common["Authorization"] = `Bearer ${token}`);
const removeToken = () =>
  (client.defaults.headers.common["Authorization"] = "");

const storeToken = (token) => {
  localStorage.setItem("token", token);
  addToken(token);
};

const clearToken = () => {
  removeToken();
  localStorage.removeItem("token");
};
  
export default {
    BASE_URL,
    get,
    post,
    remove,
    storeToken,
    clearToken,
};
  