import * as Yup  from 'yup';
import errorMessage from './errorMessages'

const phoneNumber = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;


const registerValidationSchema = Yup.object().shape({
  firstName: Yup.string()
    .required(errorMessage.required('nombre')),
    lastName: Yup.string()
    .required(errorMessage.required('apellido')),

  phone: Yup.string()
    .required(errorMessage.required('numero'))
    .test("phone", "Numero de telefono invalido", function(value){
        return phoneNumber.test(value)
  }),   

  email: Yup.string()
    .email(errorMessage.email('email'))
    .required(errorMessage.required('email')),

  password: Yup.string()
    .required(errorMessage.required('contraseña'))
    .min(8, errorMessage.min('contraseña')),

  confirmPassword: Yup.string()
    .required(errorMessage.required('contraseña'))
    .test("confirmPassword", errorMessage.equalPassword, function(value){
      return this.parent.password === value
    }),
    
});

export default registerValidationSchema;