const errorMessage = {
    required(field) {
        return `Ingrese ${field}`;
    },

    email() {
        return `Email inválido`;
    },

    min(field) {
        return `Ingresa ${field} completo`;
    },

    max(field, character) {
        return `${field} debe tener máximo ${character} caracteres`
    },
    equalPassword(){
        return `Las contraseñas deben ser iguales`
    }
}

export default errorMessage