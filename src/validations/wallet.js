import * as Yup  from 'yup';
import errorMessage from './errorMessages'

const walletValidationSchema = Yup.object().shape({
    name_wallet: Yup.string()
    .required(errorMessage.required('nombre de billetera')),
});

export default walletValidationSchema;