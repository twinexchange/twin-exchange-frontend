import * as Yup  from 'yup';
import errorMessage from './errorMessages'

const loginValidationSchema = Yup.object().shape({
  email: Yup.string()
    .email(errorMessage.email('email'))
    .required(errorMessage.required('email')),

  password: Yup.string()
    .required(errorMessage.required('contraseña')),
});

export default loginValidationSchema;