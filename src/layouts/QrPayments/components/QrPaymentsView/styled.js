import styled from 'styled-components'
import VerificationInput from "react-verification-input";

export const Container = styled.section`
    background: #F8F8FC;
    margin-bottom: 10%;
`

export const MainBody = styled.div`
    display: flex;
    width: 100%;
    justify-content: center;
`

export const QrContainer = styled.div`
    width: 40vw;
    -webkit-box-shadow: 5px 5px 13px 3px rgba(0,0,0,0.1); 
    box-shadow: 5px 5px 13px 3px rgba(0,0,0,0.1);
    border-radius: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
    overflow: hidden;
`   

export const QrCode = styled.div`
    height: 190px;
    width: 190px;
    margin-top: 5%;
    padding: 15px;
    border-radius: 10px;
    -webkit-box-shadow: inset 2px 2px 5px -1px rgba(0,0,0,0.25); 
    box-shadow: inset 2px 2px 5px -1px rgba(0,0,0,0.25);
`

export const QrCodeImage = styled.img`
    height: 100%;
    width: 100%;
`

export const QrDir = styled.p`
    color: rgba(87, 95, 107, 0.7);
    font-size: 10px;
    margin-bottom: 20px;
`

export const QrVerifyInput = styled.div`
    display: flex;
    margin-top: auto;
    height: 20%;
    width: 100%;
    justify-content: center;
    align-items: center;
    background: #F8F8FC;
    padding: 5px 0px;
`

export const QrVerificationInput = styled(VerificationInput)`
`