import React from 'react'
import SubmenuTitle from '../../../../components/titles/SubmenuTitle'
import {
    Container, 
    MainBody,
    QrContainer,
    QrCode,
    QrCodeImage,
    QrDir,
    QrVerifyInput,
    QrVerificationInput
} from './styled'
import qrExample from '../../../../images/example_qr_code.svg'
import './verificationInput.css'

export const QrPaymentsView = () => {
    return(
        <Container>
            <SubmenuTitle title={"PAGOS CON QR"} subtitle={"Ingresa tu firma electrónica"} />
            <MainBody>
                <QrContainer>
                    <QrCode>
                        <QrCodeImage src={qrExample} />
                    </QrCode>
                    <QrDir>15794usvnfm3498j29mcerg3fe5456856748</QrDir>
                    <QrVerifyInput>
                        <QrVerificationInput 
                            length={4} 
                            placeholder=" "  
                            removeDefaultStyles
                            characters={{
                                className: "characters",
                            }}
                            character={{
                                className: "character",
                                classNameInactive: "character--inactive",
                                classNameSelected: "character--selected",
                            }}
                        />
                    </QrVerifyInput>
                </QrContainer>
            </MainBody>
        </Container>
    )
}