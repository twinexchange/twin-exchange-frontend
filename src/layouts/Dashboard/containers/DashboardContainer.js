import DashboardView from '../components/DashboardView';
import { connect } from 'react-redux';
import * as actions from "../../../store/actions/profile";

const mapStateToProps = (state) => {
    const profileReducer = state.profileReducer.userData
    const profileData = state.dataReducer
    return{
        fetchUserData: profileReducer.isFetching,
        successUserData:  profileReducer.loginSuccess,
        userDataError: profileReducer.loginError,
        profileData: profileData.userData
    }
}

const mapDispatchToProps = dispatch => ({
    getProfileData: () => dispatch(actions.getProfilePending()),
})

export default connect(mapStateToProps, mapDispatchToProps)(DashboardView);