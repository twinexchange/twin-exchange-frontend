import styled from 'styled-components'
import glass_icon from '../../../../images/icons/glass_icon.svg'

export const HeaderContainer = styled.div`
    width: 95%;
    display: flex;
    justify-content: flex-end;
    padding: 25px;
    align-items: baseline;
`

export const SearchContainer = styled.div`
    width: 30%;
    display: flex;
    justify-content: center;
    flex-direction: row;
`

export const SearchInput = styled.input`
    width: 95%;
    border-radius: 20px;
    padding: 5px;
    -webkit-box-shadow: inset 5px 5px 15px 5px rgba(0,0,0,0.1); 
    box-shadow: inset 5px 5px 15px 5px rgba(0,0,0,0.1);
    background: url(${glass_icon}) no-repeat scroll 3px 3px;
    background-position: 95%;
    border: 0px;

    &::placeholder{
        padding: 5%;
        color: #99A0AE;
    }

    &:focus{
        outline: none;
    }
`

export const HeaderMenu = styled.div`
    display: flex;
    align-items: center;
`

export const MenuTitle = styled.div`
    color: #777777;
    cursor: pointer;
    margin: 0px 20px;
`

export const ProfileCard = styled.div`
    display: flex;
    flex-direction: row;
    margin-left: 20px;
`

export const ProfileCardInfo = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-end;
`

export const Name = styled.p`
    font-weight: bold;
    font-size: 20px;
    margin: 0px;
`

export const Mail = styled.p`
    font-size: 13px;
    margin: 0px;
    color: #575F6B;
`

export const Role = styled.p`
    color: #3395FF;
    cursor: pointer;
    font-weight: bold;
    font-size: 12px;
    margin: 0px;
`

export const ProfilePhoto = styled.img`
    height: 60px;
    width: 60px;
    background: blue;
    border-radius: 15px;
    margin-left: 10px;
    object-fit: cover;
`