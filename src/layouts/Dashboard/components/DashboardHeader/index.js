import React, {useState, useContext, useEffect} from 'react'
import {HeaderContainer, SearchContainer, SearchInput, HeaderMenu, MenuTitle, ProfileCard, Name, Mail, Role, ProfileCardInfo, ProfilePhoto} from './styled'
import {IconButton} from '../IconButton'
import twinLogo from '../../../../images/icons/logo twin.svg'
import calculator from '../../../../images/icons/calculator.svg'
import chat from '../../../../images/icons/chat.svg'
import defaultProfilePhoto from '../../../../images/default_profile.jpeg'     
import support from '../../../../images/icons/support_button.svg'
import notifications from '../../../../images/icons/notification.svg'
import { OptionsContext, ExchangeMoneyContext } from '../../../../Context'
import ClickOutside from '../../../../components/wrappers/ClickOutside'
import {SelectOption} from '../../../../components/ContextSelector/SelectOption'
import { LanguageSelector } from '../../../../components/ContextSelector/LanguageSelector'
import { ExchangeSelector } from '../../../../components/ContextSelector/ExchangeSelector'

export const DashboardHeader = ({userData, fetchUserData , getUserData}) => {
    
    const [showLanguageSelector, setShowLanguageSelector] = useState(false)
    const [showExchangeSelector, setShowExchangeSelector] = useState(false)
    const language = useContext(OptionsContext).language;
    const exchange = useContext(ExchangeMoneyContext).exchange;

    console.log(userData)

    useEffect(() => {
        if(!fetchUserData){
            getUserData()
        }
    }, [])

    return(
        <HeaderContainer>
            <SearchContainer>
                <SearchInput placeholder="Buscar" />
            </SearchContainer>
            <HeaderMenu>
                        {
                            showLanguageSelector && <ClickOutside clickFunction={() => setShowLanguageSelector(false)}><LanguageSelector down={true} /></ClickOutside>
                        }
                        {
                            language ? <SelectOption selected={true} item={language} onClick={() => setShowLanguageSelector(true)} /> : <MenuTitle onClick={() => setShowLanguageSelector(true)}>Idioma</MenuTitle>
                        }
                        {
                            showExchangeSelector && <ClickOutside clickFunction={() => setShowExchangeSelector(false)}><ExchangeSelector down={true} /></ClickOutside>
                        }
                        {
                            exchange ? <SelectOption selected={true} item={exchange} onClick={() => setShowExchangeSelector(true)} /> : <MenuTitle onClick={() => setShowExchangeSelector(true)}>Moneda</MenuTitle>
                        }
                <IconButton icon={twinLogo} />
                <IconButton icon={calculator} />
                <IconButton icon={chat} />
                <IconButton icon={support} />
                <IconButton icon={notifications} />
            </HeaderMenu>
            <ProfileCard>
                <ProfileCardInfo>
                    <Name>{userData && `${userData.firstName} ${userData.lastName}`}</Name>
                    <Mail>{userData && userData.email}</Mail>
                    <Role>Director</Role>
                </ProfileCardInfo>
                <ProfilePhoto src={defaultProfilePhoto} />
            </ProfileCard>
        </HeaderContainer>
    )
}