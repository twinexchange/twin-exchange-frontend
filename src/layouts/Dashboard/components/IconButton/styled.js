import styled from 'styled-components'

export const ButtonContainer = styled.div`
    height: 28px;
    margin: 0px 10px;
`

export const Icon = styled.img`
    object-fit: contain;
    height: 100%;
`