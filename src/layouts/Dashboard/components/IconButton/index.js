import React from 'react'
import {ButtonContainer, Icon} from './styled'

export const IconButton = ({icon}) => {
    return(
        <ButtonContainer>
            <Icon src={icon} />
        </ButtonContainer>
    )
}