import styled from 'styled-components'

export const DashboardContainer = styled.div`
    background: #F8F8FC;
    width: 100%;
    display: flex;
    min-height: 100vh;
`

export const DashboardMenu = styled.div`
    width: 100%;
    margin-left: 7vw;
`