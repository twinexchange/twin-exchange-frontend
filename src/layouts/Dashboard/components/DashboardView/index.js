import React, {Component} from 'react'
import {DashboardHeader} from '../DashboardHeader'
import {DashboardContainer, DashboardMenu} from './styled'
import {CryptoMenu} from '../../../CryptoMenu'
import { SideBar } from '../../../DirectoryBusiness/Components/Sidebar';

class DashboardView extends Component {

    constructor(props) {
        super(props);
    } 
    
    handleGetUserData(){
        this.props.getProfileData()
    }


    render(){
        console.log(this.props)
        return(
            <DashboardContainer>
                <SideBar logoutFunction={this.props.logout} />
                <DashboardMenu>
                    <DashboardHeader 
                        userData={this.props.profileData} 
                        getUserData={this.handleGetUserData.bind(this)} 
                        fetchUserData={this.props.fetchUserData}
                    />
                    <CryptoMenu userData={this.props.profileData} /> 
                </DashboardMenu>           
            </DashboardContainer>
        )
    }

}

export default DashboardView