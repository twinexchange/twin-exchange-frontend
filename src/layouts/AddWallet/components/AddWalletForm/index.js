import React, {useState, useEffect} from 'react'
import {
    CreateWallet, 
    FormContainer, 
    Subtitle, 
    SampleText,
    CoinSelect,
    CreateForm, 
    FormLabel, 
    FormField, 
    SubmitButton,
    CoinOption,
    CoinOptionForm,
    OptionIcon,
    OptionText,
    OptionRadioButton,
    OptionControl,
    CustomErrorMessage,
    SuccessText
} from './styled'
import {Formik, ErrorMessage} from 'formik'
import {cryptoList} from '../../../../utils/dashboardList'
import {customErrorMessage, ErrorMessageText} from '../../../../components/customErrorMessage'
import walletValidationSchema from '../../../../validations/wallet'

export const AddWalletForm = ({
    profileId, 
    createWallet,
    fetchError,
    fetchCreateWallet,
    successCreateWallet,
    errorCreateWallet,
}) => {
    const[coinIcon, setCoinIcon] = useState(cryptoList[0].icon)

    console.log(fetchCreateWallet, successCreateWallet, errorCreateWallet)

    useEffect(() => {
        console.log(fetchCreateWallet)
    }, [fetchCreateWallet])

    const initialValues = {
        name_wallet: "",
        user: profileId,
        coin: cryptoList[0].longName,
    };

    function onSubmit(values) {
        createWallet(values)
    }

    return(
        <CreateWallet>
            <Formik validationSchema={walletValidationSchema} validateOnChange={false} {...{ initialValues, onSubmit }}>
                    {({errors, setFieldValue}) => (
                <>
                    <FormContainer>
                            <Subtitle>NUEVO MONEDERO</Subtitle>
                            <CreateForm className="baseForm" noValidate>
                                <FormLabel>NOMBRE DE MONEDERO</FormLabel>
                                <ErrorMessage component={CustomErrorMessage} name="name_wallet" />
                                {
                                    errorCreateWallet && <ErrorMessageText>Ya hay un monedero con este nombre</ErrorMessageText>
                                }
                                <FormField
                                    type="name_wallet"
                                    id="name_wallet"
                                    className="name_wallet formField"
                                    name="name_wallet"
                                    placeholder="Monederos de ahorros"
                                />                             
                                <FormLabel>SELECCIONA UNA MONEDA</FormLabel>
                                <FormField
                                    type="select"
                                    id="coin"
                                    className="coin"
                                    name="coin"
                                    disabled
                                    icon={coinIcon}
                                />
                                {
                                    fetchError && <ErrorMessageText>Email o contraseña incorrectos</ErrorMessageText>
                                }
                                <SampleText>Tu monedero se guardará en la moneda seleccionada</SampleText>                               
                                <SubmitButton>CREAR MONEDERO</SubmitButton>
                                {
                                    successCreateWallet && <SuccessText>El monedero fue creado con exito 😄</SuccessText>
                                }
                            </CreateForm>
                    </FormContainer>
                    <CoinSelect>
                        <Subtitle>SELECCIONE UNA MONEDA</Subtitle>
                            <CoinOptionForm>
                                {
                                    cryptoList.map((coin) => (
                                        <CoinOption key={coin.name}>
                                            <OptionIcon src={coin.icon} />
                                            <OptionText htmlFor={"coin"}>{coin.longName}</OptionText>
                                            <OptionRadioButton
                                                type="radio"
                                                id="coin"
                                                className="coin"
                                                name="coin"
                                                icon={coin.icon}
                                                value={coin.longName}
                                                onChange={(e) => {
                                                    setFieldValue("coin", e.target.value);
                                                    setCoinIcon(coin.icon)
                                                }}
                                                defaultChecked={coin.longName === "Twincoin" ? true: false}
                                            />
                                        </CoinOption>
                                    ))
                                }
                            </CoinOptionForm>
                    </CoinSelect>
                </>    
                    )}
            </Formik>
        </CreateWallet>
    )
}