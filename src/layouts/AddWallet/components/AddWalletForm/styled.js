import styled from 'styled-components'
import {Field, Form} from 'formik'
import {customErrorMessage, ErrorMessageText} from '../../../../components/customErrorMessage'

export const CreateWallet = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 2%;
    margin-bottom: 25px;
`

export const FormContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    width: 45%;
    display: flex;
    flex-direction: column;

`

export const Subtitle = styled.span`
    font-size: 20px;
    font-weight: lighter;
    color: #99A0AF;
    text-align: center;
    display: flex;
    align-self: center;

`

export const SampleText = styled.p`
    color: #575F6B;
    font-weight: lighter;
    font-size: 12px;
    text-align: center;
    margin-top: 5%;
`

export const CoinSelect = styled.div`
    width: 25%;
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
`

export const CreateForm = styled(Form)`
    display: flex;
    flex-direction: column;
`

export const FormLabel = styled.label`
    color: #3395FC;
    margin-left: 50px;
    margin-bottom: 5px;
`

export const FormField = styled(Field)`
    padding: 12px;
    margin: 0px 50px;
    box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    outline-width: 0px;
    border: none;
    border-radius: 10px;
    margin-bottom: 5%;
    background: #ffffff;
    display: flex;
    justify-content: center;
    background: ${props => `url(${props.icon}) no-repeat scroll`};
    background-size: 20px 20px;
    background-position: 10px center;

    &.coin {
        padding-left: 6%;
        color: #575F6B;
    }
    
    
`

export const SubmitButton = styled.button`
    width: 40%;
    margin: auto;
    border: none;
    padding: 10px 15px;
    border-radius: 20px;
    font-size: 16px;
    font-weight: bold;
    color: #99A0AF;
    background: #ffffff;
    box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    margin-bottom: 5%;
    cursor: pointer;
`

export const CoinOption = styled.div`
    display: flex;
    background: #ffffff;
    justify-content: center;
    align-items: center;
    border-radius: 10px;
    padding: 3% 0px;
    margin: 3% 0px;
`

export const CoinOptionForm = styled.div`
    padding: 12%;
    border-radius: 20px;
    -webkit-box-shadow: 8px 6px 12px 5px rgba(0,0,0,0.1); 
    box-shadow: 8px 6px 12px 5px rgba(0,0,0,0.1);
`

export const OptionIcon = styled.img`
    height: 20px;
    width: 20px;
    margin: 0px 15px;
`

export const OptionText = styled.label`
    color: #575F6B;
    font-weight: lighter;
    font-size: 13px;
`

export const OptionRadioButton = styled.input`
    margin-left: auto;
    margin-right: 5%;

    &:not(:checked) {
        left: 0;
        top: 0;
        width: 1.25em;
        height: 1.25em;  
    }

    &:checked {
        width: 1.4em;
        height: 1.4em;
        border-radius: .2em;
        transition: all .275s;
    }
    
`

export const CustomErrorMessage = styled(customErrorMessage)`
    margin: 10px 0px 7px 50px
`

export const SuccessText = styled.p`
    color: #4BB543;
    font-size: 20px;
    margin: 0px;
    text-align: center;
`