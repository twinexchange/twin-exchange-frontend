import styled from 'styled-components'

export const Container = styled.section`
    margin-top: 30px;
`

export const MainTitle = styled.span`
    font-size: 30px;
    color: #99A0AF;
    margin-left: 5%; 
`

