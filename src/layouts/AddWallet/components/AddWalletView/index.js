import React, {Component} from 'react'
import {Container, MainTitle} from './styled'
import {AddWalletForm} from '../AddWalletForm'

class AddWalletView extends Component {

    constructor(props){
        super(props)
    }

    createWalletForm(data){
        this.props.createWallet(data)
    }

    render(){
        const list = true

        return(
            <Container>
                <MainTitle>AGREGAR MONEDERO</MainTitle>
                <AddWalletForm 
                    profileId={this.props.profileData.id} 
                    createWallet={this.createWalletForm.bind(this)}
                    {...this.props}
                />
            </Container>
        )
    }

}

export default AddWalletView