import AddWalletView from '../components/AddWalletView';
import { connect } from 'react-redux';
import * as actions from "../../../store/actions/wallet";

const mapStateToProps = (state) => {
    const profileReducer = state.profileReducer.userData
    const walletReducer = state.walletReducer.createWallet
    const profileData = state.dataReducer
    return{
        fetchUserData: profileReducer.isFetching,
        successUserData:  profileReducer.loginSuccess,
        userDataError: profileReducer.loginError,
        profileData: profileData.userData,

        fetchCreateWallet: walletReducer.isFetching,
        successCreateWallet: walletReducer.createSuccess,
        errorCreateWallet: walletReducer.createError
    }
}

const mapDispatchToProps = dispatch => ({
    createWallet: (values) => dispatch(actions.createWallet(values)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddWalletView);