import React from 'react'
import { FooterContainer, ButtonsContainer, MenuContainer } from './styled'
import { OptionsMenu } from '../OptionButton'
import { Link } from 'react-router-dom'

export const Footer = () => {
    return (
        <FooterContainer>
            <ButtonsContainer>
                <MenuContainer>
                    <OptionsMenu title={"Idioma"} />
                    <OptionsMenu title={"Moneda"} />
                    <Link to="/directory-business"><OptionsMenu title={"Directorio Empresarial"}> </OptionsMenu></Link>
                    
                </MenuContainer>
            </ButtonsContainer>
        </FooterContainer>
    )
}