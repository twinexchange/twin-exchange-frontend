import React, { Component } from 'react';
import { MainContainer, CardWindow, PrincipalContainer, SideBarContainer, LogoContainer, Logo } from './styled'
import { Header } from '../Header'
import { SideBar } from '../Sidebar';
import DirectoryBusinessMap from '../DirectoryBusinessMap'
import back_button from '../../../../images/directory-business/back-button.svg'

import { MapContainer, ContentContainer } from './styled'
import { DirectoryBusinessCardInfo } from '../DirectoryBusinessCardInfo';

class DirectoryBusiness extends Component {
    constructor(props) {
        super(props);
        this.scrollElements = React.createRef() 
        this.state = {
            showLoginCard: false,
        };
    }

    executeScroll = () => this.scrollElements.current.scrollIntoView()

    render() {
        console.log(this.props)
        return (
            <>

                <Header />
                <PrincipalContainer>
                    <SideBarContainer>
                        <LogoContainer>
                            <Logo src={back_button} />
                        </LogoContainer>
                    </SideBarContainer>
                    <ContentContainer ref={this.myRef}>
                    <DirectoryBusinessCardInfo  />
                    </ContentContainer>
                    <MapContainer>
                        <DirectoryBusinessMap />
                    </MapContainer>
                </PrincipalContainer>
            </>
        );
    }
}

export default DirectoryBusiness