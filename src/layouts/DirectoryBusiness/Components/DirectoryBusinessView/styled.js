import styled from 'styled-components'
import background from '../../../../images/background.png'


export const PrincipalContainer = styled.div`
    background-image: url(${background});
    height: 100vh;
    display: flex;
    flex-direction: row;
`

export const ContentContainer = styled.div`
  display: flex;
  flex-flow: column wrap;  /* align as a column and allow wrapping of content */
  align-items: center;  /* centers content horizontally */
  height: 100%; /* fixed height div */
  width: 25%;
`

export const MapContainer = styled.div`
  width: 70%;
  height: 85%;
  justify-content: center;
  border-radius: 20px;
  margin: 20px;
  right: 20px;
  border: 2px #fff solid;
`

export const SideBarContainer = styled.div`
    height: 100%;
    width: 5%;
`;

export const LogoContainer = styled.div`
    display: flex;
    flex-direction: column;
    bottom: 5px;
    right: 5px;
    align-items: center;
    cursor: pointer;
`

export const Logo = styled.img`
    width: 140%;
`;