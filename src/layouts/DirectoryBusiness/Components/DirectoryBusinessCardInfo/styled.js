import styled, { keyframes } from 'styled-components'

const fadeIn = keyframes`
  from {
    opacity: 0
  }
  to {
    opacity: 1
  }
`;

export const Container = styled.div`
   display: flex;
   height: 25%;
   width: 90%;
   margin-top: 5%;
   border-radius: 20px;
   border: 3px #fff solid;
    box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.25);
    -webkit-box-shadow: 2px 4px 8px 0px rgba(0,0,0,0.07);
    -moz-box-shadow: 2px 4px 48px 0px rgba(0,0,0,0.05);
    flex-direction: column;
    align-items: center;
    animation: ${fadeIn} 0.25s linear;
`

export const BackgroundImage = styled.img`
  display: flex;
  justify-content: center;
  align-content: center;
  align-self: center;
  height: 50%;
  width: 100%;
`

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
`