import React from 'react'
import {
    HeaderContainer,
    TwinBusinessChatContainer,
    TwinBusinessChatLogo,
    SearchContainer,
    SearchInput,
    TitleHeaderContainer,
    TitleHeader,
    ButtonsContainer,
    MenuContainer,
    Support,
    SupportImage,
} from './styled'
import { RoundedButton } from '../../../../components/buttons/RoundedButton'
import { OptionsMenu } from '../OptionButton'
import support_button from '../../../../images/icons/support_button.svg'
import twin_logo from '../../../../images/directory-business/twin-business.svg'

export const Header = () => {

    return (
        <>
            <HeaderContainer>
                <TwinBusinessChatContainer>
                    <TwinBusinessChatLogo src={twin_logo} />
                </TwinBusinessChatContainer>
                <SearchContainer>
                    <SearchInput placeholder="Buscar" />
                </SearchContainer>
                {/**
                <TitleHeaderContainer>
                    <TitleHeader>Directorio Empresarial</TitleHeader>
                </TitleHeaderContainer>
                <ButtonsContainer>
                    <OptionsMenu title={"Ingresar"} />
                </ButtonsContainer> */}
            </HeaderContainer>
        </>
    )
}



{/**
<MenuContainer>
                    <OptionsMenu title={"Ingresar"} onClick={() => console.log("hello")} />
                    <Support>
                        <SupportImage src={support_button} alt="support" />
                    </Support>
                </MenuContainer> */}