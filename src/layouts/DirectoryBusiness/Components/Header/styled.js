import styled from 'styled-components'
import glass_icon from '../../../../images/icons/glass_icon.svg'


export const HeaderContainer = styled.div`
    /**
    background-color: #000;
    opacity: 0.5; */
    display: flex;
    flex-direction: row;

`;

export const TwinBusinessChatContainer = styled.div`
    width: 60px;
    height: 60px;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
`

export const TwinBusinessChatLogo = styled.img`
    height: 100%;
    width: 100%;
    bottom: 10px;
`

export const SearchContainer = styled.div`
    width: 30%;
    display: flex;
    justify-content: center;
    flex-direction: row;
`

export const SearchInput = styled.input`
    width: 95%;
    height: 20%;
    border-radius: 20px;
    padding: 5px;
    align-self: center;
    top: 5px;
    -webkit-box-shadow: inset 5px 5px 15px 5px rgba(0,0,0,0.1); 
    box-shadow: inset 5px 5px 15px 5px rgba(0,0,0,0.1);
    background: url(${glass_icon}) no-repeat scroll 3px 3px;
    background-position: 95%;
    border: 0px;

    &::placeholder{
        padding: 5%;
        color: #99A0AE;
    }

    &:focus{
        outline: none;
    }
`

export const TitleHeaderContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-content: center;
    align-self: center;
    width: 25%;
    height: 20%;
`

export const TitleHeader = styled.h3`
    font-size: 25px;
    color: #3395FF;
    justify-content: center;
    align-content: center;
    align-self: center;
    align-items: center;
`

export const ButtonsContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    align-content: flex-end;
    align-self: center;
    padding-right: 5%;
    margin-top: 10px;
    margin-bottom: 10px;
`;

export const MenuContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 30%;
`;

export const Support = styled.div`
    width: 60px;
    height: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const SupportImage = styled.img`
    height: 60%;
    width: 60%;
    cursor: pointer;
`
