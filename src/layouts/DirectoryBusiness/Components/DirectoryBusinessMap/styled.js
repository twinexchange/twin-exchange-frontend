import styled from 'styled-components'

export const ViewMap = styled.div`
    height: 100%;
    width: 100%;
    border-radius: 20px;
    overflow: hidden;
`