import styled from 'styled-components'

export const OptionButton = styled.div`
    color: #c3c8ce;
    cursor: pointer;
    font-weight: 600;

    &:hover {
        color: #000000
    }
`