import React, {useState} from 'react'
import { 
    SideBarContainer, 
    ExternalSidebar,
    LogoContainer, 
    Logo,
    BackButton,
    VerticalBarContainer,
    VerticalBar,
    VerticalBarBox,
    MainMenuContainer,
    MainMenu,
    CloseButton,
    NavigationContainer,
    Close,
} from './styled'
import SideBarNavigation from '../../../../components/navigation/SidebarNavigation'
import logo from '../../../../images/imagotipo_twin.svg'
import back_button from '../../../../images/directory-business/back-button.svg'
import close from '../../../../images/icons/close.svg'

export const SideBar = ({logoutFunction}) => {
    const [displayMenu, setDisplayMenu] = useState(false)
    const menuTitle = "Menú \n De herramientas"

    return (
        <>
            <SideBarContainer>
                <ExternalSidebar onClick={() => setDisplayMenu(true)}>
                    <LogoContainer>
                        <Logo src={logo} />
                        <BackButton src={back_button} />
                        <VerticalBarContainer>
                            <VerticalBarBox>
                                {menuTitle}
                            </VerticalBarBox>
                            <VerticalBar />
                        </VerticalBarContainer>
                    </LogoContainer>
                </ExternalSidebar>
            </SideBarContainer>
    
            <MainMenuContainer show={displayMenu}>
                <MainMenu>
                    <Logo src={logo} />
                    <CloseButton onClick={() => setDisplayMenu(false)}>
                        <Close src={close} />
                    </CloseButton>
                    <NavigationContainer>
                        <SideBarNavigation logoutFunction={logoutFunction} />
                    </NavigationContainer>
                </MainMenu>
            </MainMenuContainer>
        </>
    )
}