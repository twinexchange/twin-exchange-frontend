import styled, {keyframes, css} from 'styled-components'

const moveToRight = keyframes`
  from {
    transform: translateX(-100%);
  }

  to {
    transform: translateX(0%);
  }
`;

export const SideBarContainer = styled.div`
    flex-direction: column;
    height: 100%;
    position: fixed;
    background: transparent;
    align-items: center;
    padding: 10px 0px;
  
    &::-webkit-scrollbar {
        display: none;
    }
`;

export const ExternalSidebar = styled.div`
    width: 5vw;
`

export const LogoContainer = styled.div`
    display: flex;
    flex-direction: column;
    bottom: 5px;
    right: 5px;
    align-items: center;
    cursor: pointer;
    height: 100%;
`

export const Logo = styled.img`
    width: 80px;
    height: 80px;
    margin-top: 10%
`;

export const BackButton = styled.img`
    width: 80%;
    height: 80px;
    object-fit: none;
    margin-right: 15px
`

export const VerticalBarContainer = styled.div`
    width: 100%;
    position: absolute;
    top: 45%;
    display: flex;
    height: 50px;
    align-items: center;
`

export const VerticalBar = styled.div`
    height: 100%;
    width: 8px;
    background: #3395FF;
    align-self: flex-end;
    border-radius: 20px;
    z-index: 3;
    transition: all 0.25s;
    margin-left: auto;

    ${ExternalSidebar}:hover & {
        transform: rotate(180deg);
        transition: all 0.25s 
    }
`

export const VerticalBarBox = styled.div`
    background: #FFFFFF;
    width: 120px;
    position: absolute;
    height: 100%;
    margin-left: 50px;
    padding: 20px 40px;
    padding-right: 0px;
    z-index: 1;
    border-radius: 15px;
    white-space: pre-line;
    align-items: center;
    display: flex;
    transition: all 0.25s;
    opacity: 0;
    font-size: 15px;
    color: #3395FF;

    ${ExternalSidebar}:hover & {
        opacity: 1;
        transition: all 0.35s;
        width: 120px;
        padding-right: 30px;
    }
`

export const MainMenuContainer = styled.div`
    flex-direction: column;
    height: 100%;
    position: fixed;
    background: transparent;
    align-items: center;
    padding: 0px;
    width: 15vw;
    overflow-y: scroll; 
    transform: ${props => props.show === true ? 'translateX(0%)' : 'translateX(-100%)'};
    transition: transform 0.25s ease;

    &::-webkit-scrollbar {
        display: none;
    }
`

export const MainMenu = styled.div`
    display: flex;
    flex-direction: column;
    height: 120%;
    background: #FFFFFF;
    padding: 0px;
    width: 100%;
    overflow-y: scroll; 
    margin: 20px 0px;
    position: absolute;
    top: 0;
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
    align-items: center;

    &::-webkit-scrollbar {
        display: none;
    }

`

export const CloseButton = styled.div`
    position: absolute;
    margin-top: 15%;
    right: 0;
    margin-right: 10%;
    cursor: pointer;
`

export const Close = styled.img`
   height: 100%;
`

export const NavigationContainer = styled.div`
    margin: 10% 0px;
    border-bottom: 1px solid #B2B2B2;
`