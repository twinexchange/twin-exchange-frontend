/* eslint-disable no-unused-vars */
import LandingView from '../Components/LandingView';
import { connect } from 'react-redux';
import * as actions from "../../../store/actions/auth";

const mapStateToProps = (state) => {
    const authReducerLogin = state.authReducer.login
    return{
        fetchLogin: authReducerLogin.isFetching,
        successLogin:  authReducerLogin.loginSuccess,
        loginError: authReducerLogin.loginError
    }
}

const mapDispatchToProps = dispatch => ({
    login: (values) => dispatch(actions.loginPending(values)),

    register: (values) => dispatch(actions.registerPending(values)),
})

export default connect(mapStateToProps, mapDispatchToProps)(LandingView);