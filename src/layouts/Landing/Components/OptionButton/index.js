import React from 'react'
import {OptionButton} from './styled'

export const OptionsMenu = ({title, onClick}) => {
    return(
        <OptionButton onClick={onClick}>
            {title}
        </OptionButton>
    )
}