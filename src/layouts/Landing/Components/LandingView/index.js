import React, { Component } from 'react';

import { Footer } from '../Footer';
import { Header } from '../Header';
import { LogoTitle } from '../LogoTitle';
import { CardWindow, MainContainer } from './styled';


class LandingView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoginCard: false,
            showRegisterCard: false,
        };
      }    

    toggleLoginCard(){
        this.setState({
            showLoginCard: true,
            showRegisterCard: false
        })
        
    }  

    toggleRegisterCard(){
        this.setState({
            showRegisterCard: true,
            showLoginCard: false
        })
    } 

    outsideClick(){
        this.setState({
            showRegisterCard: false,
            showLoginCard: false
        })
    }

    handleLogin(values){
        this.props.login(values);
    }

    handleRegister(values){
        this.props.register(values)
    }

    render(){
        return (
           <MainContainer>
                <Header 
                    toggleLogin={this.toggleLoginCard.bind(this)} 
                    toggleRegister={this.toggleRegisterCard.bind(this)} 
                    outsideClick={this.outsideClick.bind(this)} 
                    showLogin={this.state.showLoginCard} 
                    showRegister={this.state.showRegisterCard} 
                    handleLogin={this.handleLogin.bind(this)} 
                    handleRegister={this.handleRegister.bind(this)}  
                    fetchLogin={this.props.fetchLogin}
                    loginError={this.props.loginError}
                    successLogin={this.props.successLogin}
                />
                <LogoTitle />
                <Footer changeLanguage={this.props.changeLanguage} />
                {
                    this.state.showLoginCard || this.state.showRegisterCard ? <CardWindow/> : null                
                }
           </MainContainer>
          );
    }
}

export default LandingView