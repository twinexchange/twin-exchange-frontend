import styled, {keyframes} from 'styled-components'
import background from '../../../../images/background.png'

const fadeIn = keyframes`
  from {
    opacity: 0
  }
  to {
    opacity: 1
  }
`;

export const MainContainer = styled.div`
    height: 100%;
    width: 100%;
    background-image: url(${background});
    position: absolute
`;

export const CardWindow = styled.div`
    height: 100%;
    width: 100%;
    position: absolute;
    background: rgba(0,0,0,0.10);
    top: 0px;
    pointer-events: none;
    animation: ${fadeIn} 0.25s linear;
`