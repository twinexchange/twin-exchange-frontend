import styled, {keyframes} from 'styled-components'
const fadeIn = keyframes`
  from {
    opacity: 0
  }
  to {
    opacity: 1
  }
`;

export const CardContainer = styled.div`
    background: #F8F8FC;
    position: absolute;
    margin-top: 80px;
    margin-right: 5%;
    border-radius: 30px;
    right: 7%;
    z-index: 1;
    justify-content: center;
    animation: ${fadeIn} 0.25s linear;
    

    &::before{
        content: '';
        position: absolute;
        transform: ${props => props.loginMode === true ? "translate(50px, -100%)" : "translate(210px, -100%)"};
        border-width: 0 15px 25px 15px;
        border-color: transparent transparent #F8F8FC transparent;
        border-style: solid;
    };
`;

export const MainContainer = styled.div`
    padding: 20px 45px;
    overflow: auto;
    height: 100%;
    max-height: 72vh;

    
    &::-webkit-scrollbar{
        display: none;
    }
`

export const Title = styled.h1`
    color: #0F83FF;
    font-size: 25px;
    text-transform: uppercase;
    margin-top: 0px;
`

export const MainForm = styled.div`

`
