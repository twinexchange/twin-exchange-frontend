import React from 'react'
import { CardContainer, MainContainer, MainForm, Title } from './styled'

export const AuthCard = ({title, children, loginMode}) => {

    return(
        <CardContainer loginMode={loginMode} >
            <MainContainer>
                <Title>{title}</Title>
                <MainForm>
                    {children}
                </MainForm>
            </MainContainer>
        </CardContainer>
    )
}