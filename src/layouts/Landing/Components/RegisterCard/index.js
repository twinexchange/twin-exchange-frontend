import React from 'react'
import {AuthCard} from '../AuthCard'
import { Formik, Form , ErrorMessage} from "formik";
import {CustomField} from '../../../../components/form/CustomField'
import {CustomSelect} from '../../../../components/form/CustomSelect'
import {IconRoundedButton} from '../../../../components/buttons/IconRoundedButton'
import {OptionText, OptionLabel, RadioContainer, RadioField, TermsAndConditions} from './styled'
import registerValidationSchema from '../../../../validations/register'
import {customErrorMessage} from '../../../../components/customErrorMessage'
import DatePickerField from "../../../../components/form/DatePicker";

export const RegisterCard = ({handleRegister}) => {

    const initialValues = {
        firstName: "",
        lastName: "",
        phone: "",
        email: "",
        password: "",
        confirmPassword: "",
        born_date: "",
        gender: "",
        profession: ""
      };
    
    function onSubmit(values) {
        handleRegister(values)
    }  

    return(
        <AuthCard title={'Crear Cuenta'}>
            <Formik validationSchema={registerValidationSchema} validateOnChange={false} {...{ initialValues, onSubmit }}>
                {({errors, values, setFieldValue}) => (
                <Form className="baseForm" noValidate>
                    <CustomField
                        type="firstName"
                        id="firstName"
                        className="firstName formField"
                        name="firstName"
                        placeholder="Nombre"
                        error={errors.firstName}
                    />
                    <ErrorMessage component={customErrorMessage} name="name" />

                    <CustomField
                        type="lastName"
                        id="lastName"
                        className="lastName formField"
                        name="lastName"
                        placeholder="Apellido"
                        error={errors.lastName}
                    />
                    <ErrorMessage component={customErrorMessage} name="surname" />

                    <CustomField
                        type="phone"
                        id="phone"
                        className="phone formField"
                        name="phone"
                        placeholder="Celular"
                        error={errors.phone}
                    />
                    <ErrorMessage component={customErrorMessage} name="phone" />

                    <CustomField
                        type="email"
                        id="email"
                        className="email formField"
                        name="email"
                        placeholder="Correo electrónico"
                        error={errors.email}
                    />
                    <ErrorMessage component={customErrorMessage} name="email" />

                    <CustomField
                        type="password"
                        id="password"
                        className="password formField"
                        name="password"
                        placeholder="Contraseña"
                        textBellow="Minimo 8 caracteres"
                    />
                    <ErrorMessage component={customErrorMessage} name="password" />

                    <CustomField
                        type="password"
                        id="confirmPassword"
                        className="confirmPassword formField"
                        name="confirmPassword"
                        placeholder="Confirmar contraseña"
                        error={errors.confirmPassword}
                    />
                    <ErrorMessage component={customErrorMessage} name="confirmPassword" />
                    
                    <DatePickerField name="born_date" placeholder="Fecha de nacimiento" />
                    <ErrorMessage component={customErrorMessage} name="born_date" />

                    <CustomSelect
                        type="gender"
                        id="gender"
                        className="gender formField"
                        name="gender"
                        placeholder="Genero"
                        value={values.gender}
                        onChange={(e) => setFieldValue('gender', e.target.value)}
                    >
                        <option value="" selected disabled hidden>Genero</option>
                        <option value="male">Hombre</option>
                        <option value="female">Mujer</option>
                    </CustomSelect>
                    <ErrorMessage component={customErrorMessage} name="genre" />

                    <CustomField
                        type="profession"
                        id="profession"
                        className="profession formField"
                        name="profession"
                        placeholder="Profesión"
                    />
                    <ErrorMessage component={customErrorMessage} name="profession" />
                    <OptionText>
                        Para compras en línea, ingresa tus datos de facturación
                    </OptionText>
                    <RadioContainer>
                        <OptionLabel>
                            <RadioField type="radio" name="picked" value={true} />
                            SI
                        </OptionLabel>
                        <OptionLabel>
                            <RadioField type="radio" name="picked" value={false} />
                            NO
                        </OptionLabel>
                    </RadioContainer>
                    {
                        /*
                        <OptionTitle>
                            Seleccione una opción
                        </OptionTitle>
                        */
                    }
                    <OptionText>
                        Al presionar en crear cuenta aceptas nuestros <TermsAndConditions href="https://www.google.com.ar/">TERMINOS Y CONDICIONES</TermsAndConditions>
                    </OptionText>
                    <IconRoundedButton type="submit" text={'Crear Cuenta'} backgroundColor={'#3395FF'} onClick={() => onSubmit()} />
                </Form>
                )}
            </Formik>
        </AuthCard>
    )
}