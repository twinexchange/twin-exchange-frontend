import styled from 'styled-components'
import { Field } from "formik";

export const OptionTitle = styled.p`
    color: #FD0D1B;
    font-weight: bold;
    font-size: 14px;
`

export const RadioContainer = styled.div`
    display: flex;
    justify-content: center;
    align-content: center;
`

export const OptionText = styled.p`
    color: #0F83FF;
    font-size: 12px;
    display: flex;
    flex-direction: column;
    width: 18vw;
`;

export const OptionLabel = styled.label`
    margin: 0px 5%;
    background: #ffffff;
    width: 50%;
    padding: 10px 0px;
    border-radius: 10px;
    text-align: center;
    font-weight: bold;
    -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
    box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
    color: #0F83FF;

    &:checked{
        background: red;
    }
`

export const RadioField = styled(Field)`
    &[type="radio"]{
            display: none;
            margin: 10px;
            &:checked:before{
                content:"";
                font: 17px/1 'Open Sans', sans-serif;
                position:absolute;
                width: 100%;
                height: 100%;
                background:orange;
                border-radius: 100%;
                left: 0;
                top: 0;
            }
    }
`

export const TermsAndConditions = styled.a`
    color: #0F83FF;
    font-weight: bold;
`