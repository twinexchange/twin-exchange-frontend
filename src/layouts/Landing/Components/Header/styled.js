import styled from 'styled-components'

export const HeaderContainer = styled.div`
    margin-top: 3%;
`;

export const ButtonsContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    padding-right: 5%;
    margin-top: 10px;
    margin-bottom: 10px;
`;

export const MenuContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 30%;
`;

export const Support = styled.div`
    width: 60px;
    height: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
`

export const SupportImage = styled.img`
    height: 70%;
    width: 70%;
    cursor: pointer;
`
