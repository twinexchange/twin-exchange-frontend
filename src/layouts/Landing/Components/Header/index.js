import React from 'react'
import {HeaderContainer, ButtonsContainer ,MenuContainer, Support, SupportImage} from './styled'
import {RoundedButton} from '../../../../components/buttons/RoundedButton'
import {OptionsMenu} from '../OptionButton'
import support_button from '../../../../images/icons/support_button.svg'
import {LoginCard} from '../LoginCard'
import {RegisterCard} from '../RegisterCard'
import ClickOutside from '../../../../components/wrappers/ClickOutside'

export const Header = ({
    toggleLogin, 
    toggleRegister, 
    outsideClick, 
    showLogin, 
    showRegister, 
    handleLogin, 
    handleRegister,
    //fetchLogin,
    loginError,
    successLogin,
}) => {
    
    return(
        <HeaderContainer>
            <ButtonsContainer>
                <MenuContainer>
                    <OptionsMenu  title={"Ingresar"} onClick={() => toggleLogin()} />
                    <RoundedButton backgroundColor={'#3395ff'} title={"Crear Cuenta"} hoverBackgroundColor={'#3384ff'} onClick={() => toggleRegister()} />
                    <Support>
                        <SupportImage src={support_button} alt="support" />
                    </Support>
                  
                </MenuContainer>
                {
                    showLogin && !showRegister &&<ClickOutside clickFunction={() => outsideClick()}><LoginCard handleLogin={handleLogin} fetchError={loginError} successLogin={successLogin} /></ClickOutside> 
                }
                {
                    !showLogin && showRegister && <ClickOutside clickFunction={() => outsideClick()}><RegisterCard handleRegister={handleRegister} /></ClickOutside> 
                }
            </ButtonsContainer>
        </HeaderContainer>
    )
}