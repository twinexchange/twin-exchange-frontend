import styled from 'styled-components'

export const Container = styled.div`
    height: 70vh;
    width: 100%;
    justify-content: center;
    align-items: center;
    display: flex;
`;

export const LogoContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    cursor: pointer;
`

export const Logo = styled.img`
    width: 80%;
`;

export const About = styled.p`
    color: #c3c8ce;
    font-weight: 600;
`