import React from 'react'
import {Container, Logo, LogoContainer, About} from './styled'
import logo from '../../../../images/logo.svg'

export const LogoTitle = () => {
    return(
        <Container>
            <LogoContainer>
                <Logo src={logo} alt="logo_twin_exchange" />
                <About>¿Quienes Somos?</About>
            </LogoContainer>
        </Container>
    )
}