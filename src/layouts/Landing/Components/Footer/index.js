import React, {useContext, useState} from 'react'
import {FooterContainer, ButtonsContainer ,MenuContainer, OptionContainer} from './styled'
import {OptionsMenu} from '../OptionButton'
import { LanguageSelector } from '../../../../components/ContextSelector/LanguageSelector'
import { ExchangeSelector } from '../../../../components/ContextSelector/ExchangeSelector'
import {SelectOption} from '../../../../components/ContextSelector/SelectOption'
import { OptionsContext, ExchangeMoneyContext } from '../../../../Context'
import ClickOutside from '../../../../components/wrappers/ClickOutside'
import { Link } from 'react-router-dom'

export const Footer = () => {
    const [showLanguageSelector, setShowLanguageSelector] = useState(false)
    const [showExchangeSelector, setShowExchangeSelector] = useState(false)
    const language = useContext(OptionsContext).language;
    const exchange = useContext(ExchangeMoneyContext).exchange;

    return(
        <FooterContainer>
            <ButtonsContainer>
                <MenuContainer>
                    <OptionContainer onClick={() => setShowLanguageSelector(true)}>
                        {
                            showLanguageSelector && <ClickOutside clickFunction={() => setShowLanguageSelector(false)}><LanguageSelector /></ClickOutside>
                        }
                        {
                            language ? <SelectOption selected={true} item={language} onClick={() => setShowLanguageSelector(true)} /> : <OptionsMenu title={"Idioma"} onClick={() => setShowLanguageSelector(true)}/>   
                        }
                    </OptionContainer>
                    <OptionContainer>
                        {
                            showExchangeSelector && <ClickOutside clickFunction={() => setShowExchangeSelector(false)}><ExchangeSelector /></ClickOutside>
                        }
                        {
                            exchange ? <SelectOption selected={true} item={exchange} onClick={() => setShowExchangeSelector(true)} /> : <OptionsMenu title={"Moneda"} onClick={() => setShowExchangeSelector(true)}/>   
                        }
                    </OptionContainer>
                    <OptionContainer>
                        <Link to='/directory-business' style={{ textDecoration: 'none' }} ><OptionsMenu title={"Directorio Empresarial"} /></Link>
                    </OptionContainer>
                </MenuContainer>
            </ButtonsContainer>
        </FooterContainer>
    )
}