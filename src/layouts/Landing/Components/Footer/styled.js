import styled from 'styled-components'

export const FooterContainer = styled.div`
    margin-top: 2%;
`;

export const ButtonsContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    padding-left: 5%;
`;

export const OptionContainer = styled.div`
    display: flex;
    justify-content: center;
    height: 50px;
    align-items: center;
`

export const MenuContainer = styled.div`
    width: 40%;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const Support = styled.div`
    width: 60px;
    height: 60px;
    background: blue;
`
