import React, {useEffect} from 'react'
import {AuthCard} from '../AuthCard'
import {RegisterMessage, RecoverPassword} from './styled'
import {IconRoundedButton} from '../../../../components/buttons/IconRoundedButton'
import { Formik, Form, ErrorMessage } from "formik";
import {CustomField} from '../../../../components/form/CustomField'
import twinLogo from '../../../../images/icons/twinbusiness logo.svg'
import facebookLogo from '../../../../images/icons/icono_facebook.svg'
import googleLogo from '../../../../images/icons/icono_google.svg'
import loginValidationSchema from '../../../../validations/login'
import {customErrorMessage, ErrorMessageText} from '../../../../components/customErrorMessage'
import { useHistory } from "react-router-dom";

export const LoginCard = ({handleLogin, fetchError, successLogin}) => {
    let history = useHistory();

    const initialValues = {
        email: "",
        password: "",
    };
    
    function onSubmit(values) {
        handleLogin(values)
    }  

    useEffect(() => {
        successLogin === true && history.push("/app/twincoin")
    }, [successLogin])

    return(
        <AuthCard title={'Ingresar'} loginMode={true}>
            <Formik validationSchema={loginValidationSchema} validateOnChange={false} {...{ initialValues, onSubmit }}>
                {({errors}) => (
                <Form className="baseForm" noValidate>
                    {
                        fetchError && <ErrorMessageText>Email o contraseña incorrectos</ErrorMessageText>
                    }
                    <CustomField
                        type="email"
                        id="email"
                        className="email formField"
                        name="email"
                        placeholder="Correo electrónico o teléfono"
                        error={errors.email}
                    />
                    <ErrorMessage component={customErrorMessage} name="email" />
                    
                    <CustomField
                        type="password"
                        id="password"
                        className="password formField"
                        name="password"
                        placeholder="Contraseña"

                    />
                    <ErrorMessage component={customErrorMessage} name="password" />

                    <IconRoundedButton text={'Iniciar sesión'} backgroundColor={'#3395FF'} type="submit" />
                </Form>
                )}
            </Formik>
            <IconRoundedButton text={'Iniciar con Twinbusiness'} textColor={"#3395FF"} logo={twinLogo} />
            <IconRoundedButton text={'Iniciar con Facebook'} textColor={"#3395FF"} logo={facebookLogo} />
            <IconRoundedButton text={'Iniciar con Google'} textColor={"#3395FF"} logo={googleLogo} />
            <RegisterMessage>
                ¿No tienes cuenta?
                <b>Regístrate</b>
            </RegisterMessage>
            <RecoverPassword>
                Recuperar contraseña
            </RecoverPassword>
        </AuthCard>
    )
}