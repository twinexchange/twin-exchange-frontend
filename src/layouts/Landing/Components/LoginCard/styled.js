import styled from 'styled-components'
import { Field } from "formik";

export const RegisterMessage = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    height: 80px;
    -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
    box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
    background: #ffffff;
    border-radius: 10px;
    font-size: 12px;
    color: #0F83FF;
    cursor: pointer;
`;

export const RecoverPassword = styled.p`
    font-size: 12px;
    color: #0F83FF;
    text-align: center;
    cursor: pointer;
    margin: 20px 0px; 
`


export const FormikField = styled(Field)`
    width: 18vw;
    height: 40px;
    border: 0px;
    border-radius: 10px;
    -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
    box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
    text-align: center;

    &::placeholder{
        color: #3395FF;
        text-align: initial;
        font-weight: bold;
        padding: 0px 10%;
    }
`;