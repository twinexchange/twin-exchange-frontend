import React from 'react'
import {Container, Nav} from './styled'
import {CryptoOption} from './components/CryptoOption'
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { cryptoList } from '../../utils/dashboardList'
import { MainMenu } from './components/MainMenu'
import addWallet from '../../images/icons/add_wallet.svg'
import AppWallet from '../AddWallet'

export const CryptoMenu = ({userData}) => {
    return(
        <BrowserRouter>
            <Container>
                <Nav to={`/app/${"agregar"}`} key={"Agregar monedero"} activeClassName={"selected"}>
                    <CryptoOption title={"Agregar monedero"} icon={addWallet} />
                </Nav>
                {
                    cryptoList.map(({name, icon, goTo, ammount}) => 
                        <Nav to={`/app/${goTo}`} key={name} activeClassName={"selected"}>
                            <CryptoOption title={name} icon={icon} ammount={ammount} />
                        </Nav>
                    )
                }               
            </Container>
            <Switch>
                <Route exact key={"agregar"} path={`/app/${"agregar"}`} component={() => <AppWallet  />} />
                {
                    cryptoList.map(({name, goTo, borderGraphColor, pointBackgroundColor, icon, longName}) => 
                        <Route exact key={name} path={`/app/${goTo}`} component={() => 
                            <MainMenu 
                                name={name} 
                                icon={icon} 
                                borderGraphColor={borderGraphColor} 
                                cryptoUrl={goTo} 
                                longName={longName} 
                                pointBackgroundColor={pointBackgroundColor}/>
                        }/>
                    )
                }
            </Switch>
         </BrowserRouter>
    )
}
