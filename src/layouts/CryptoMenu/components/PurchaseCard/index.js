import React from 'react'
import { Formik, Form, ErrorMessage } from "formik";
import { 
    Container, 
    FormikField, 
    Title, 
    Subtitle, 
    SliderContainer, 
    Mark, 
    SubmitButton, 
    PurchaseHistoryContainer, 
    HistoryList,
    HistoryText 
} from './styled'
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

export const PurchaseCard = ({onSubmit, name, theme}) => {

    const initialValues = {
        price: "",
        quantity: "",
        total: "",
    };
    
    return(
        <Container background={theme.background}>
            <Title>{theme.keyword} {name}</Title>
            <Subtitle>Balance {name}</Subtitle>
            <Formik validateOnChange={false} {...{ initialValues, onSubmit }}>
                {({errors}) => (
                <Form className="baseForm" noValidate>
                    <FormikField
                        type="input"
                        className="price formField"
                        name="price"
                        placeholder="Precio"
                        error={errors.email}
                    />
                
                    
                    <FormikField
                        type="input"
                        className="quantity formField"
                        name="quantity"
                        placeholder="Cantidad"
                    />
                    <SliderContainer>
                        <Slider 
                            min={0} 
                            max={100} 
                            defaultValue={3} 
                            marks={{ 0: <Mark>0%</Mark>, 50: <Mark>50%</Mark>, 100: <Mark>100%</Mark> }} 
                            dotStyle={{
                                background: `${theme.sliderColor}`,
                                border: "none",
                                height: "6px"
                            }}
                            handleStyle={{
                                padding: "5px",
                                background: `${theme.slideSelectColor}`,
                                border: "none",
                                height: "20px",
                                width: "20px",
                            }}
                            railStyle={{
                                background: `${theme.sliderColor}`,
                                height: "6px"
                            }}
                            trackStyle={{
                                border: `2px solid ${theme.sliderColor}`,
                                height: "6px",
                                background: "#FFFFFF",
                            }}
                            activeDotStyle={{
                                background: `${theme.sliderColor}`,
                            }}
                            
                        />
                    </SliderContainer>
                    <FormikField
                        type="input"
                        className="total formField"
                        name="total"
                        placeholder="Total"

                    />
                    <SubmitButton background={theme.buttonBackground}>{theme.keyword}</SubmitButton>
                </Form>
                )}
            </Formik>
        </Container>
    )
}

export const PurchaseHistory = ({theme}) => (
    <PurchaseHistoryContainer>
        <Title>{theme.altName}</Title>
        <HistoryList background={theme.background}>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
            <HistoryText>$28,480,955.00</HistoryText>
        </HistoryList>
    </PurchaseHistoryContainer>
)