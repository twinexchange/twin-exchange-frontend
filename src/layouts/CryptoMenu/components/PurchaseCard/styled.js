import styled from 'styled-components'
import { Field } from 'formik'

export const Container = styled.div`
    width: 43%;
    background: ${props => props.background && props.background};
    padding: 5% 3%;
    border-radius: 20px;
    display: flex;
    justify-content: center;
    display: flex;
    flex-direction: column;
    align-items: center;

`

export const FormikField = styled(Field)`
    width: 85%;
    height: 30px;
    border: 0px;
    border-radius: 20px;
    -webkit-box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05); 
    box-shadow: 0px 10px 28px 5px rgba(0,0,0,0.05);
    margin: 8px 5%;

    &::placeholder{
        color: #898F97;
        text-align: initial;
        padding: 0px 5%;
    }
    &:focus{
        outline: none
    }
`;

export const Title = styled.span`
    font-weight: bolder;
    font-size: 25px;
    margin: 0px 10px 5px 10px;
    color: #898F97;
`

export const Subtitle = styled.span`
    font-weight: bolder;
    font-size: 15px;
    color: #898F97;
    display: flex;
    align-self: flex-start;
    margin: 5% 10%;
`

export const SliderContainer = styled.div`
    padding: 5% 7%;
    margin: 5px 0px 15px 0px;
`

export const Mark = styled.small`
    font-weight: bold;
    font-size: 15px;
    color: #707070;
`

export const SubmitButton = styled.button`
    display: flex;
    margin: auto;
    margin-top: 5%;
    padding: 5% 15%;
    border: none;
    border-radius: 20px;
    font-weight: bolder;
    color: #FFFFFF;
    background: ${props => props.background && props.background};
    line-height: 20px;
    font-size: 15px;
`

export const PurchaseHistoryContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 49%;
    justify-content: center;
    text-align: center;
`

export const HistoryList = styled(Container)`
    width: auto;
    justify-content: flex-start;
    margin-top: 5%;
    overflow: scroll;
    height: 260px;
    padding-top: 20px;
    overflow-x: hidden;
    
    &::-webkit-scrollbar{
        display: none;
    }
`

export const HistoryText = styled.p`
    font-size: 14px;
    font-weight: lighter;
`