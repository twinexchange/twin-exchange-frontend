import styled from 'styled-components'

export const Footer = styled.section`
    display: flex;
    flex-direction: column;
`

export const Title = styled.span`
    font-weight: bolder;
    font-size: 25px;
    margin: 0px 10px;
    color: #898F97;
`

export const Container = styled.div`
    width: 98%;
    margin: 2% 2.5vw;
    border-radius: 20px;
    display: flex;
    align-self: center;
    -webkit-box-shadow: 0px 0px 20px 4px rgba(230,230,230,0.8); 
    box-shadow: 0px 0px 20px 4px rgba(230,230,230,0.8);
    padding: 1% 0px;
`

export const MainTable = styled.table`
    width: 100%;
    text-align: center;
    border-collapse: collapse;
    font-weight: lighter;
    color: #878E97;
`

export const TableHead = styled.tr`
    border-bottom: 2px solid #dcdee0;
`

export const HeadTitle = styled.p`
    font-weight: lighter;
    color: #878E97;
`

export const Row = styled.tr`
    height: 80px;
`

export const CryptoImage = styled.img`
    height: 20px;
    width: 20px;
    margin-right: 5%;
`

export const WebImage = styled.img`
    margin-right: 10px;
`

export const WebData = styled.div`
    display: flex;
    justify-content: center;
`

export const WebText = styled.a`
    text-decoration: none;
    color: #878E97;
`