import React from 'react'
import {
    Footer, 
    Title, 
    Container, 
    MainTable, 
    TableHead, 
    HeadTitle, 
    Row,
    CryptoImage,
    WebImage,
    WebData,
    WebText
} from './styled'
import webIcon from '../../../../images/icons/webIcon.svg'
import {cryptoList} from '../../../../utils/dashboardList'

export const OpenMarketsFooter = () => {
    return(
        <Footer>
            <Title>MERCADOS ABIERTOS</Title>
            <Container>
                <MainTable>
                    <thead>
                        <TableHead>
                            <th><HeadTitle>MERCADO</HeadTitle></th>
                            <th><HeadTitle>INFORMACIÓN</HeadTitle></th>
                            <th><HeadTitle>PRECIO</HeadTitle></th>
                            <th><HeadTitle>VOLUMEN</HeadTitle></th>
                            <th><HeadTitle>CAMBIO EN 24HRS</HeadTitle></th>
                        </TableHead>
                    </thead> 
                    
                    <tbody>
                        <Row>
                            <td>
                                <WebData>
                                    <CryptoImage src={cryptoList[0].icon}/>
                                    <WebText>{cryptoList[0].name}</WebText>
                                </WebData>
                            </td>
                            <td>
                                <WebData>
                                    <WebImage src={webIcon}/>
                                    <WebText href={"https://www.google.com.ar/"}>www.vfdnvoei</WebText>
                                </WebData>
                            </td>
                            <td>$951.31</td>
                            <td>$28,480,955.00</td>
                            <td>-1.43%</td>
                        </Row>
                        <Row>
                            <td>
                                <WebData>
                                    <CryptoImage src={cryptoList[1].icon}/>
                                    <WebText>{cryptoList[1].name}</WebText>
                                </WebData>
                            </td>
                            <td>
                                <WebData>
                                    <WebImage src={webIcon}/>
                                    <WebText href={"https://www.google.com.ar/"}>www.vfdnvoei</WebText>
                                </WebData>
                            </td>
                            <td>$951.31</td>
                            <td>$28,480,955.00</td>
                            <td>-1.43%</td>
                        </Row>
                        <Row>
                            <td>
                                <WebData>
                                    <CryptoImage src={cryptoList[2].icon}/>
                                    <WebText>{cryptoList[2].name}</WebText>
                                </WebData>
                            </td>
                            <td>
                                <WebData>
                                    <WebImage src={webIcon}/>
                                    <WebText href={"https://www.google.com.ar/"}>www.vfdnvoei</WebText>
                                </WebData>
                            </td>
                            <td>$951.31</td>
                            <td>$28,480,955.00</td>
                            <td>-1.43%</td>
                        </Row>
                        <Row>
                            <td>
                                <WebData>
                                    <CryptoImage src={cryptoList[3].icon}/>
                                    <WebText>{cryptoList[3].name}</WebText>
                                </WebData>
                            </td>
                            <td>
                                <WebData>
                                    <WebImage src={webIcon}/>
                                    <WebText href={"https://www.google.com.ar/"}>www.vfdnvoei</WebText>
                                </WebData>
                            </td>
                            <td>$951.31</td>
                            <td>$28,480,955.00</td>
                            <td>-1.43%</td>
                        </Row>
                        <Row>
                            <td>
                                <WebData>
                                    <CryptoImage src={cryptoList[4].icon}/>
                                    <WebText>{cryptoList[4].name}</WebText>
                                </WebData>
                            </td>
                            <td>
                                <WebData>
                                    <WebImage src={webIcon}/>
                                    <WebText href={"https://www.google.com.ar/"}>www.vfdnvoei</WebText>
                                </WebData>
                            </td>
                            <td>$951.31</td>
                            <td>$28,480,955.00</td>
                            <td>-1.43%</td>
                        </Row>
                        <Row>
                            <td>
                                <WebData>
                                    <CryptoImage src={cryptoList[5].icon}/>
                                    <WebText>{cryptoList[5].name}</WebText>
                                </WebData>
                            </td>
                            <td>
                                <WebData>
                                    <WebImage src={webIcon}/>
                                    <WebText href={"https://www.google.com.ar/"}>www.vfdnvoei</WebText>
                                </WebData>
                            </td>
                            <td>$951.31</td>
                            <td>$28,480,955.00</td>
                            <td>-1.43%</td>
                        </Row>
                    </tbody>
                </MainTable>
            </Container>
        </Footer>
    )
}
