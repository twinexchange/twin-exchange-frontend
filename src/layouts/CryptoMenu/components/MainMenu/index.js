import React from 'react'
import {Container} from './styled'
import {ActionMenu} from '../ActionMenu'


export const MainMenu = ({name, borderGraphColor, pointBackgroundColor, icon, cryptoUrl, longName}) => {
    const statisticsData = {
        labels: ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
        datasets: [
          {
            data: [0.0025, 0.002, 0.0017, 0.0025, 0.003, 0.0028, 0.0025, 0.0036, 0.0042, 0.0045, 0.004, 0.0035],
            pointRadius: 10,
            fill: false,
            backgroundColor: "rgba(75,192,192,0.2)",
            pointBackgroundColor: "#5D93FB",
            borderColor: borderGraphColor
          },
        ]
      };

    return(
        <Container>
            <ActionMenu 
              name={name} 
              borderGraphColor={borderGraphColor} 
              icon={icon} longName={longName} 
              pointBackgroundColor={pointBackgroundColor} 
            />
        </Container>
        
    )
}