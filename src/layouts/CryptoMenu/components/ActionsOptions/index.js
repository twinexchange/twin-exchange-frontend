import React from 'react'
import {IconOption, Title} from './styled'
import {OptionContainer} from '../ActionMenu/styled'

export const ActionsOptions = ({icon, title}) => {
    return(
        <OptionContainer>
            <IconOption src={icon} />
            <Title>{title}</Title>
        </OptionContainer>
    )
}
