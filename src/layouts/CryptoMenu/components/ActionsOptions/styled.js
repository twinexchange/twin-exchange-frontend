import styled from 'styled-components'

export const IconOption = styled.img`
    height: 20px;
`

export const Title = styled.p`
    font-size: 12px;
    color: #898E97;
    margin: 0px 0px 0px 8px;
`