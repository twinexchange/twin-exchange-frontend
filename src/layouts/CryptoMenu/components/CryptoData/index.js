import React, {useState} from 'react'
import {
    Container,
    MainBody,
    MainInfoContainer, 
    CryptoGraphContainer, 
    GraphDataContainer, 
    DataContainer, 
    IconData, 
    DataTitle,
    GraphOptions, 
    MarketOptions,
    Statistics, 
    TradeHistory, 
    DateOptions, 
    Date,
    Title,
    OrdersContainer,
    OrdersSelect,
    OrdersOptions,
    ActiveOrders,
    OrdersHistory,
    OrderDescription,
    OrderTitle,
    BottomLine,

    MainPurchaseContianer,
    Limit,
    Market,
    PurchaseOptions,
    BookOfOrdersTitle,
} from './styled'
import { Line } from "react-chartjs-2";
import { chartGeneralOptions } from '../../../../utils/chartOptions'
import { PurchaseCard, PurchaseHistory } from '../PurchaseCard'
import { OpenMarketsFooter } from '../OpenMarketsFooter'

export const CryptoData = ({name, pointBackgroundColor, borderGraphColor, icon}) => {

    const [graphMode, setGraphMode] = useState("statistics")
    const [orderMode, setOrderMode] = useState("active")
    const [dateMode, setDateMode] = useState("year")

    const [purchaseMode, setPurchaseMode] = useState("limit")

    const statisticsData = {
        labels: ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
        datasets: [
          {
            data: [0.0025, 0.002, 0.0017, 0.0025, 0.003, 0.0028, 0.0025, 0.0036, 0.0042, 0.0045, 0.004, 0.0035],
            pointRadius: 10,
            fill: false,
            backgroundColor: "rgba(75,192,192,0.2)",
            pointBackgroundColor: pointBackgroundColor,
            borderColor: borderGraphColor
          },
        ]
      };

    const theme = {
        sell: {
            background: "rgba(255, 79, 14, 0.2)",
            keyword: "VENDER",
            buttonBackground: "#FD0D1B",
            sliderColor: "#FF0000",
            slideSelectColor: "linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(255,0,0,1) 100%)",
            altName: "VENDIDO"
        },

        buy: {
            background: "rgba(93, 147, 251, 0.2)",
            keyword: "COMPRAR",
            buttonBackground: "#3395FC",
            sliderColor: "#5D93FB",
            slideSelectColor: "linear-gradient(90deg, rgba(255,255,255,1) 0%, rgba(93,147,251,1) 100%)",
            altName: "COMPRADO"
        }
    };
    
    function onSubmit(values) {
        console.log(values)
    }  

    return(
        <Container>
            <MainBody>
                <MainInfoContainer>     
                    <CryptoGraphContainer>
                        <GraphDataContainer>
                            <DataContainer>
                                <IconData src={icon} />
                                <DataTitle>{name}</DataTitle>
                            </DataContainer>
                            <GraphOptions>
                                <Statistics onClick={() => setGraphMode("statistics")} mode={graphMode}>ESTADÍSTICAS</Statistics>
                                <TradeHistory onClick={() => setGraphMode("history")} mode={graphMode}>HISTORIA DE TRADE</TradeHistory>
                            </GraphOptions>
                        </GraphDataContainer>
                        <DateOptions>
                            <Date onClick={() => setDateMode("day")} selected={dateMode === "day" ? true : false}>1D</Date>
                            <Date onClick={() => setDateMode("week")} selected={dateMode === "week" ? true : false}>1W</Date>
                            <Date onClick={() => setDateMode("month")} selected={dateMode === "month" ? true : false}>1M</Date>
                            <Date onClick={() => setDateMode("year")} selected={dateMode === "year" ? true : false}>1Y</Date>
                        </DateOptions>
                        <Line data={statisticsData} options={chartGeneralOptions} />
                    </CryptoGraphContainer>
                    <Title>MIS ÓRDENES</Title>
                    <OrdersContainer>
                        <OrdersSelect>
                            <OrdersOptions>
                                <ActiveOrders onClick={() => setOrderMode("active")} mode={orderMode}>ÓRDENES ACTIVAS</ActiveOrders>
                                <OrdersHistory onClick={() => setOrderMode("history")} mode={orderMode}>HISTORIAL</OrdersHistory>
                            </OrdersOptions>
                        </OrdersSelect>
                        <OrderDescription>
                            <OrderTitle>TIEMPO</OrderTitle>
                            <OrderTitle>ID DE TRADE</OrderTitle>
                            <OrderTitle>ID DE ORDEN</OrderTitle>
                            <OrderTitle>PRECIO</OrderTitle>
                            <OrderTitle>MONTO</OrderTitle>
                            <OrderTitle>TOTAL</OrderTitle>
                        </OrderDescription>
                        <BottomLine />
                    </OrdersContainer>
                </MainInfoContainer>  
                <MainPurchaseContianer>
                    <OrdersOptions>
                        <Limit onClick={() => setPurchaseMode("limit")} mode={purchaseMode}>LÍMITE</Limit>
                        <Market onClick={() => setPurchaseMode("market")} mode={purchaseMode}>MERCADO</Market>
                    </OrdersOptions>
                    <PurchaseOptions>
                        <PurchaseCard name={name} theme={theme.sell} />
                        <PurchaseCard name={name} theme={theme.buy} />
                    </PurchaseOptions>
                    <BookOfOrdersTitle>LIBRO DE ÓRDENES</BookOfOrdersTitle>
                    <PurchaseOptions>
                        <PurchaseHistory theme={theme.sell} />
                        <PurchaseHistory theme={theme.buy} />
                    </PurchaseOptions>
                </MainPurchaseContianer>
            </MainBody>
            <OpenMarketsFooter />
        </Container>
    )
}