import styled from 'styled-components'

export const Container = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
`

export const MainBody = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-around;
`

export const MainInfoContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    margin: 5vh 0px;
    flex-direction: column;
`

export const CryptoGraphContainer = styled.div`
    width: 45vw;
    padding: 2.5vw;
    border-radius: 15px;
    background: #F8F8FC;
    -webkit-box-shadow: 0px 0px 20px 4px rgba(230,230,230,0.8); 
    box-shadow: 0px 0px 20px 4px rgba(230,230,230,0.8);
`

export const GraphDataContainer = styled.div`
    display: flex;
    align-items: center;
    margin: 0px 0px 20px 0px;
    justify-content: center;
`

export const DataContainer = styled.div`
    height: 60px;
    display: flex;
    align-items: center;
    margin-right: auto;
`

export const IconData = styled.img`
    height: 20px;
`

export const DataTitle = styled.span`
    font-weight: bolder;
    font-size: 25px;
    margin: 0px 10px;
    color: #898F97;
`

export const GraphOptions = styled.div`
    display: flex;
    position: absolute;
    margin: auto;
    padding: 10px 15px;
    -webkit-box-shadow: 0px 0px 20px 4px rgba(0,0,0,0.1); 
    box-shadow: 0px 0px 20px 4px rgba(0,0,0,0.1);
    border-radius: 10px;
    color: #898F97
`

export const OptionView = styled.div`
    padding: 8px 5px;
    border-radius: 10px;
    min-width: 160px;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
`

export const Statistics = styled(OptionView)`
    margin-right: 10px;
    box-shadow: ${(props) => props.mode === "statistics" ? "inset 3px 3px 15px 0px rgba(0,0,0,0.1)" : "none"};
`

export const TradeHistory = styled(OptionView)`
    margin-left: 10px; 
    box-shadow: ${(props) => props.mode === "history" ? "inset 3px 3px 15px 0px rgba(0,0,0,0.1)" : "none"};
`

export const DateOptions = styled.div`
    display: flex;
    justify-content: flex-end;
    margin: 20px 0px;
`

export const Date = styled.button`
    color: #898F97;
    margin: 0px 5px;
    cursor: pointer;
    border: none;
    background: transparent;
    -webkit-box-shadow: inset 3px 3px 15px 0px rgba(0,0,0,0.05); 
    box-shadow: inset 3px 3px 15px 0px rgba(0,0,0,0.05);
    box-shadow: ${(props) => props.selected ? "inset 3px 3px 3px 1px rgba(0,0,0,0.05);" : "none"};
    width: fit-content;
    padding: 1px;
    border-radius: 5px;

    &:focus{
        outline: 0 !important;
    }
`

export const Title = styled(DataTitle)`
    margin: 5% 0px 3% 0px;
`

export const OrdersContainer = styled(CryptoGraphContainer)`
    width: 50vw;
    height: -webkit-fill-available;
    padding: 30px 0px;
`

export const OrdersSelect = styled(GraphDataContainer)`
    margin: 0px;
`

export const OrdersOptions = styled(GraphOptions)`
    margin-bottom: 5%;
    position: relative;
`

export const ActiveOrders = styled(OptionView)`
    margin-left: 10px; 
    box-shadow: ${(props) => props.mode === "active" ? "inset 3px 3px 15px 0px rgba(0,0,0,0.1)" : "none"};
`

export const OrdersHistory = styled(OptionView)`
    margin-left: 10px; 
    box-shadow: ${(props) => props.mode === "history" ? "inset 3px 3px 15px 0px rgba(0,0,0,0.1)" : "none"};
`

export const OrderDescription = styled.div`
    display: flex;
    justify-content: space-around;
`

export const OrderTitle = styled.span`
    color: #898F97;
    font-size: 1vw;
`

export const BottomLine = styled.div`
    border-bottom: 2px solid #dcdee0;
    margin-top: 1%
`

export const MainPurchaseContianer = styled(CryptoGraphContainer)`
    width: 35vw;   
    max-height: 100%;
    margin: 5vh 0px;
`

export const Limit = styled(OptionView)`
    margin-left: 10px; 
    box-shadow: ${(props) => props.mode === "limit" ? "inset 3px 3px 15px 0px rgba(0,0,0,0.1)" : "none"};
    width: 50%;
`

export const Market = styled(OptionView)`
    margin-left: 10px; 
    box-shadow: ${(props) => props.mode === "market" ? "inset 3px 3px 15px 0px rgba(0,0,0,0.1)" : "none"};
    width: 50%;
`

export const PurchaseOptions = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
`

export const BookOfOrdersTitle = styled(Title)`
    display: flex;
    justify-content: center;
    margin-top: 15%
`
