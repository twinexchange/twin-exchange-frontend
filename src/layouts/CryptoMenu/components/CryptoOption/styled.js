import styled from 'styled-components'

export const CryptoData = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 20px;
    justify-content: center;
    color: #878E97;
    max-width: 50px;
`

export const CryptoLogo = styled.img`
    height: 50px;
    width: 50px;
`

export const CryptoTitle = styled.span`
font-size: 9px;
`

export const CryptoAmmount = styled.span`
font-size: 7px;
`