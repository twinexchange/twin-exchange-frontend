import React from 'react'
import { CryptoData ,CryptoLogo, CryptoTitle, CryptoAmmount} from './styled'
import {CryptoOptionContainer} from '../../styled'

export const CryptoOption = ({title, icon, ammount}) => {
    return(
        <CryptoOptionContainer selected={false}>
            <CryptoLogo src={icon} />
            <CryptoData>
                <CryptoTitle>{title}</CryptoTitle>
                {
                    ammount && <CryptoAmmount>{ammount}</CryptoAmmount>
                }
            </CryptoData>
        </CryptoOptionContainer>
    )
}
