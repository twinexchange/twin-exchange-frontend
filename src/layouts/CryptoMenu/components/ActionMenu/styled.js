import styled from 'styled-components'
import {NavLink} from 'react-router-dom'

export const ActionsContainer = styled.div`
    padding: 20px 30px;
    display: flex;
    justify-content: center;
`

export const Nav = styled(NavLink)`
    text-decoration: none;

    &.deactivated{
        pointer-events: none;
    }
`


export const OptionContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    cursor: pointer;
    border-radius: 10px;
    margin: 0px 10px 20px 10px;
    min-width: 110px;

    ${Nav}.selected & {
        -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15), inset 5px 4px 3px 2px rgba(0,0,0,0.1); 
        box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15), inset 5px 4px 3px 2px rgba(0,0,0,0.1);
        transition: box-shadow 0.15s ease-in-out
    }

    ${Nav}.deactivated & {
        box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
        -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
        filter: opacity(0.5)
    }
`