import React from 'react'
import {ActionsContainer, Nav} from './styled'
import { ActionsOptions } from '../ActionsOptions'
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import {actionList, cryptoList, processActionMenu} from '../../../../utils/dashboardList'
import {CryptoData} from '../CryptoData'
import { createBrowserHistory } from "history";
import WalletScreen from '../../../Wallet'
import QrPaymentsScreen from '../../../QrPayments'
import SendScreen from '../../../Send' 

export const ActionMenu = (props) => {
    const history = createBrowserHistory()
    const menuList = processActionMenu(props.name)
    
    return(
        <BrowserRouter>
            <ActionsContainer>
                {
                    menuList.map(({title, icon, goTo, deactivated}) => {
                        return(
                        <Nav to={deactivated ? `${history.location.pathname}` : `${history.location.pathname}/${goTo}`}  
                            key={title} activeClassName={"selected"} className={deactivated && "deactivated"}
                        >
                            <ActionsOptions icon={icon} title={title} />
                        </Nav>
                        )
                    }
                        
                    )
                }
            </ActionsContainer>
            <Switch>
                <Route exact path={`/app/:crypto`} component={() => <CryptoData {...props} />} />
                <Route exact path={`/app/:crypto/monedero`} component={() => <WalletScreen {...props} />} />
                <Route exact path={`/app/:crypto/pagosQR`} component={() => <QrPaymentsScreen {...props} />} />
                <Route exact path={`/app/:crypto/enviar`} component={() => <SendScreen {...props} />} />
                {
                    actionList.map(({goTo}) => <Route exact key={goTo} path={`/app/:crpyto/${goTo}`} component={ActionsOptions} />)
                } 
            </Switch>   
         </BrowserRouter>
    )
}
