import styled from 'styled-components'
import {NavLink} from 'react-router-dom'

export const Container = styled.div`

    display: flex;
    justify-content: center;
`

export const Nav = styled(NavLink)`
    text-decoration: none;
`

export const CryptoOptionContainer = styled.div`
    display: flex;
    padding: 15px 1.5vw;
    box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15);
    width: fit-content;
    border-radius: 10px;
    cursor: pointer;
    margin: 0px 10px;

    ${Nav}.selected & {
        -webkit-box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15), inset 5px 4px 3px 2px rgba(0,0,0,0.1); 
        box-shadow: 0px 0px 48px -10px rgba(0,0,0,0.15), inset 5px 4px 3px 2px rgba(0,0,0,0.1);
        transition: box-shadow 0.15s ease-in-out
    }
`