import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import Landing from './Landing'
import DirectoryBusiness from './DirectoryBusiness'
import Dashboard from './Dashboard'
import {withRouter} from 'react-router'
import { hashHistory } from 'react-router';

const Router = (props) => {

    const logout = () => {
        console.log("logout")
        localStorage.removeItem("token")
    }

    return(
        <Switch>
            <Route exact path="/" >
                <Landing />
                { !localStorage.getItem("token") ?  '' : <Redirect from="/" to="/app/twincoin" /> }
            </Route>
            <Route exact path="/directory-business" component={DirectoryBusiness} />
            <Route exact path="/" component={() => <Landing changeLanguage={props.changeLanguage} />} />
            <Route path="/app">
                <Dashboard logout={logout} />
                { !localStorage.getItem("token") ? <Redirect to="/" /> : '' }
            </Route>
        </Switch>
    )
}

export default withRouter(Router);