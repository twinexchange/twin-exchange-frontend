import React, {useState} from 'react'
import SelectDropdown from '../../../../components/dropdown/SelectDropdown'
import { 
    Container,
    FormContainer,
    FormInput,
    Input,
    InputText,
    Button,
    ButtonImage,
    FormButton
} from './styled'
import {Formik, ErrorMessage} from 'formik'
import GenericTitle from '../../../../components/titles/GenericTitle'
import ContactDropdown from '../../../../components/dropdown/ContactDropdown'
import copy from '../../../../images/icons/copy.svg'

export const TwinUsers = ({coinShortName}) => {

    console.log(coinShortName)

    const initialValues = {
        wallet: '',
        quantity: 0.00,
        sendTo: '',
        uriDirection: 'advknweiorngtpw4508rgwh349r8hfq34iofnipuq3enw',
    }

    const onSubmit = (values) => {
        console.log(values)
    }

    return(
        <Container>
            <Formik validateOnChange={false} {...{ initialValues, onSubmit }}>
                    {({errors, setFieldValue}) => (
                <>
                    <FormContainer>
                        <GenericTitle title={"Elige tu monedero"} />
                        <SelectDropdown  />

                        <GenericTitle title={"Cantidad"} />
                        <FormInput>
                            <Input
                                id="quantity"
                                className="quantity"
                                name="quantity"
                                type="text"
                            />
                            <InputText>{coinShortName}</InputText>
                        </FormInput>
                        
                        <GenericTitle title={"Destinatario"} />
                        <ContactDropdown  />

                        <GenericTitle title={"Dirección URI"} />
                        <FormInput>
                            <Input
                                id="uriDirection"
                                className="uriDirection"
                                name="uriDirection"
                                type="text"
                            />
                            <Button>
                                <ButtonImage src={copy} />
                            </Button>
                        </FormInput>
                        <FormButton type="submit">ENVIAR</FormButton>
                     </FormContainer>
                </>    
                    )}
            </Formik>
        </Container>
    )
}