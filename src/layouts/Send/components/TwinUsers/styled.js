import styled from 'styled-components'
import {Field, Form} from 'formik'

export const Container = styled.section`
    display: flex;
    flex-direction: row;
    width: 100%;
    justify-content: center;
    margin-top: 5%;
`

export const FormContainer = styled(Form)`
    width: 50%;
    top: 100%;
    align-self: center;
`

export const FormInput = styled.div`
    height: 50px;
    display: flex;
    border: none;
    box-shadow: 5px 5px 6px 2px rgba(0,0,0,0.1);
    border-radius: 15px;
    background: #ffffff;
    overflow: hidden;
`

export const Input = styled(Field)`
    width: 90%;
    border: none;
    padding-left: 20px;
    color: #99A0AF;
    outline: none;
`

export const InputText = styled.p`
    width: 10%;
    text-align: center;
    background: none;
    color: #99A0AF;
`

export const Button = styled.div`
    display: flex;
    -webkit-box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05); 
    box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05);
    width: 30px;
    height: 30px;
    padding: 10px;
    border-radius: 15px;
    background: #ffffff;
    position: absolute;
    left: 78%;
    cursor: pointer;
    justify-content: center;
    align-items: center;
`

export const ButtonImage = styled.img`

`

export const FormButton = styled.button`
    width: 40%;
    display: flex;
    transform: translateY(40px);
    margin: 0px auto;
    border: none;
    background: #F8F8FC;
    text-align: center;
    -webkit-box-shadow: 4px 4px 5px 5px rgba(0,0,0,0.7); 
    box-shadow: 4px 4px 5px 5px rgba(0,0,0,0.07);
    justify-content: center;
    font-weight: bold;
    color: rgba(87, 95, 107, 0.7);
    padding: 10px 0px;
    border-radius: 20px;
    font-size: 16px;
    outline: none;
    cursor: pointer;
    z-index: 0;
`