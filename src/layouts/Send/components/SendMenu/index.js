import React, {useState} from 'react'
import {ButtonContainer, Button, ButtonLink} from './styled'
import { cryptoList } from '../../../../utils/dashboardList'
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import SubmenuTitle from '../../../../components/titles/SubmenuTitle'
import { createBrowserHistory } from "history";

export const SendMenu = ({longName, menuSelect}) => {
    return(
        <ButtonContainer>
            <Button onClick={() => menuSelect('USUARIOS TWIN')}>  
                <ButtonLink >
                USUARIOS TWIN
                </ButtonLink>                   
            </Button>
            <Button 
                deactivatedStyle={longName !== cryptoList[1].longName ? true : false}
                onClick={() => menuSelect(longName !== cryptoList[1].longName ? '' : 'TRANSFERIR A TARJETA Ó CUENTA BANCARIA')}
            >  
                <ButtonLink>
                    TARJETA DÉBITO
                </ButtonLink>                   
            </Button>
            <Button 
                deactivatedStyle={longName === cryptoList[1].longName ? true : false}
                onClick={() => menuSelect(longName === cryptoList[1].longName ? '' : 'ENVIAR A CUENTA DE CRIPTOMONEDAS')}
            >
                <ButtonLink>
                    CUENTA DE CRIPTOMONEDAS
                </ButtonLink> 
            </Button>
            <Button 
                deactivatedStyle={longName !== cryptoList[1].longName ? true : false}
                onClick={() => menuSelect(longName !== cryptoList[1].longName ? '' : 'MÉTODOS DE ENVÍO CON SPEI')}
            >
                <ButtonLink>
                    ENVÍOS CON SPEI
                </ButtonLink> 
            </Button>
          
        </ButtonContainer>
    )
}