import React, {useState} from 'react'
import SubmenuTitle from '../../../../components/titles/SubmenuTitle'
import {
    Container, 
    ButtonContainer,
    Button,
    ButtonLink,
    SubmenuContainer
} from './styled'
import { SendMenu } from '../SendMenu'
import { TwinUsers } from '../TwinUsers'

export const SendView = ({longName, name}) => {
    const [sendMode, setSendMode] = useState('')
    console.log(name)

    const renderOptions = () => {
        switch(sendMode){
            //Yes is not clean, sorry for this :(
            case 'USUARIOS TWIN':
                return <TwinUsers coinShortName={name} />;

            case 'TRANSFERIR A TARJETA Ó CUENTA BANCARIA':
                return <div>debito</div>;

            case 'ENVIAR A CUENTA DE CRIPTOMONEDAS':
                return <div>cuentaDeCriptos</div>;

            case 'MÉTODOS DE ENVÍO CON SPEI':
                return <div>spei</div>;

            default:
                return <SendMenu longName={longName} menuSelect={setSendMode} />
        }
    }

    return(
        <Container>
            <SubmenuContainer>
                <SubmenuTitle title={"ENVIAR"} subtitle={"MÉTODOS DE ENVÍO"} textBehind={sendMode} />
            </SubmenuContainer>
            {
                renderOptions()
            }
        </Container>
    )
}