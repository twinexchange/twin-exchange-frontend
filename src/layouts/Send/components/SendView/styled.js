import styled from 'styled-components'

export const Container = styled.section`
    background: #F8F8FC;
    margin-bottom: 10%;
    display: flex;
    flex-direction: column;

`

export const ButtonContainer = styled.div`
    width: 30%;
    display: flex;
    flex-direction: column;
    align-self: center;
`

export const ButtonLink = styled.div`
    color: #FFFFFF;
    text-decoration: none;
    cursor: ${props => props.deactivatedStyle ? "default" : "pointer"};
    height: 100%;
    width: 100%;
    padding: 15px 0px; 
`

export const Button = styled.div`
    margin: 20px 0px;
    background: ${props => props.deactivatedStyle ? "#EDEDF2" : "#3395FC"};
    border-radius: 30px;
    text-align: center;
    font-weight: bold;
    font-size: 25px;
    -webkit-box-shadow: 0px 5px 15px 5px rgba(0,0,0,0.1); 
    box-shadow: 0px 5px 15px 5px rgba(0,0,0,0.1);
    cursor: ${props => props.deactivatedStyle ? "default" : "pointer"};
    color: #FFFFFF;
    display: flex;
    flex: 1;
    overflow: hidden;

    ${ButtonLink}{
        cursor: ${props => props.deactivatedStyle ? "default" : "pointer"};
    }
`

export const SubmenuContainer = styled.div`
    position: absolute;
`