import WalletView from '../components/WalletView';
import { connect } from 'react-redux';
import * as actions from "../../../store/actions/wallet";

const mapStateToProps = (state) => {
    const profileData = state.dataReducer
    const walletList = state.walletReducer.getWallets
    const deleteWallet = state.walletReducer.deleteWallet

    return{
        profileData: profileData.userData,
        
        fetchGetWalletList: walletList.isFetching,
        successGetWalletList: walletList.getSuccess,
        errorCreateWallet: walletList.getError,
        walletList: walletList.wallets,

        fetchDeleteWallet: walletList.isFetching,
        deleteError: walletList.getSuccess,
        deleteSuccess: walletList.getError,
    }
}

const mapDispatchToProps = dispatch => ({
    getWallets: (values) => dispatch(actions.getWallets(values)),
    deleteWallet: (values) => dispatch(actions.deleteWallet(values)),
})

export default connect(mapStateToProps, mapDispatchToProps)(WalletView);