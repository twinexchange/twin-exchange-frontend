import React, {useState} from 'react'
import {
    Container, 
    MainBody,
    MainList,
    WalletOption,
    ItemContainer,
    Icon,
    TextContainer,
    ItemTitle,
    ItemSubtitle,
    AvailableBalanceContainer,
    AvailableBalance,
    DataIcon,
    IconButton,
} from './styled'
import trashIcon from "../../../../images/icons/trash.svg"
import information from "../../../../images/icons/information.svg"
import {WalletInfo} from "../WalletInfo"
import SubmenuTitle from '../../../../components/titles/SubmenuTitle'

const WalletList = ({name, icon, shortName, userData, walletList, deleteWallet}) =>{
    const [infoSelected, setInfoSelected] = useState(true)
    const [listIndex, setListIndex] = useState(0)
    const [infoWalletSelected, setInfoWalletSelected] = useState(walletList[0])
 
    const changeInfoSubMenu = (index) => {
        setListIndex(index)
        setInfoSelected(true)
        setInfoWalletSelected(walletList[index])
    }

    console.log(walletList)

    const changeDelete = (userId, walletId, walletIndex, cryptoName) => {
        deleteWallet({
            userId: userId, 
            walletId: walletId, 
            walletIndex: walletIndex, 
            coin: cryptoName
        })
    }

    return(
        <Container>
            <SubmenuTitle name={name} title={"MONEDERO"} subtitle={"SOLICITUD PARA RECIBIR"} />
            <MainBody>
                <MainList>
                    {
                        walletList.map((item, index) => (
                            <WalletOption key={index}>
                                <ItemContainer>
                                    <Icon src={icon} />
                                    <TextContainer>
                                        <ItemTitle>{item.name_wallet}</ItemTitle>
                                        <ItemSubtitle>{item.type_coin}</ItemSubtitle>
                                    </TextContainer>
                                    <AvailableBalanceContainer>
                                        <ItemSubtitle>Saldo Disponible</ItemSubtitle>
                                        <AvailableBalance>{item.balance}</AvailableBalance>
                                    </AvailableBalanceContainer>
                                </ItemContainer>
                                <IconButton className={infoSelected && index === listIndex ? "selected" : null}
                                    onClick={() => changeInfoSubMenu(index)} >
                                    <DataIcon src={information} />
                                </IconButton>
                                <IconButton className={!infoSelected && index === listIndex ? "selected" : null} 
                                    onClick={() => changeDelete(userData.id, item.id, index, name)} >
                                    <DataIcon src={trashIcon} />
                                </IconButton>       
                            </WalletOption>
                        ))
                    }
                </MainList>
                <WalletInfo 
                    cryptoName={shortName} 
                    cryptoIcon={icon} 
                    walletInfo={infoWalletSelected} 
                    {...userData} 
                />
            </MainBody>        
        </Container>
    )
}

export default WalletList