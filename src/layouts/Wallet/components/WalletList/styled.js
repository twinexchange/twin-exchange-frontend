import styled from 'styled-components'

export const Container = styled.section`
    display: flex;
    width: 100%;
    flex-direction: column;
`

export const MainBody = styled.div`
    display: flex;
    justify-content: space-around;
`

export const MainList = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 25px;
    height: 70vh;
    overflow: scroll;
    padding: 0px 20px;
    
    &::-webkit-scrollbar{
        display: none;
    }
`

export const WalletOption = styled.div`
    display: flex;
    margin: 20px 0px;
`

export const ItemContainer = styled.div`
    display: flex;
    -webkit-box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05); 
    box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05);
    width: 40vw;
    padding: 10px;
    border-radius: 15px;
`

export const Icon = styled.img`
    height: 40px;
    width: 40px;
    margin-right: 5%; 
`

export const TextContainer = styled.div`
    display: flex;
    flex-direction: column;
`

export const ItemTitle = styled.p`
    margin: 0px;
    color: #99A0AF;
`

export const AvailableBalanceContainer = styled(TextContainer)`
    margin-left: auto;
    margin-right: 5%;
`

export const ItemSubtitle = styled(ItemTitle)`

`

export const AvailableBalance = styled.span`

`
export const DataIcon = styled.img`
    height: 20px;
    width: 20px;
`

export const IconButton = styled.div`
    margin-left: 2.25%;
    display: flex;
    align-self: center;
    padding: 8px; 
    box-shadow: 3px 3px 6px 2px rgba(0,0,0,0.1);
    -webkit-box-shadow: 3px 3px 6px 2px rgba(0,0,0,0.1);
    border-radius: 10px;
    cursor: pointer;
    transition: box-shadow 0.15s ease;

    &.selected{
        box-shadow: inset 1.75px 1.75px 2px 2px rgba(0,0,0,0.1);
        -webkit-box-shadow: inset 1.75px 1.75px 2px 2px rgba(0,0,0,0.1);
    }
`

