import React, {useState} from 'react'
import {
    Container,
    Header,
    DataTitle,
    ButtonsContainer,
    QrContainer,
    QrTitleAndImage,
    QrCode,
    NameAndId,
    PublicKey,
    DataBox,
    DataText,
    ButtonText,
    ExchangeContainer,
    CryptoChange,
    CryptoIcon,
    CryptoAmmount,
    ChangeDataText,
} from './styled'
import {DataIcon, IconButton} from '../WalletList/styled'
import shared from '../../../../images/icons/shared.svg'
import copy from '../../../../images/icons/copy.svg'
import exampleQr from '../../../../images/example_qr_code.svg'

export const WalletInfo = ({
    firstName, 
    lastName, 
    cryptoName, 
    cryptoIcon,
    walletInfo
}) => {
    return(
        <Container>
            <Header>
                <DataTitle><b>{`${firstName} ${lastName}`}</b></DataTitle>
                <ButtonsContainer>
                    <IconButton>
                        <DataIcon src={shared} />
                    </IconButton>
                    <IconButton>
                        <DataIcon src={copy} />
                    </IconButton>
                </ButtonsContainer>
            </Header>
            <DataTitle>Nombre del monedero</DataTitle>
            <QrContainer>
                <NameAndId>
                    <DataBox>
                        <DataText>{walletInfo.name_wallet}</DataText>
                    </DataBox>
                    <DataTitle><b>ID</b></DataTitle>
                    <DataBox>
                        <DataText>{walletInfo.id}</DataText>
                    </DataBox>
                </NameAndId>
                <QrTitleAndImage>
                    <DataTitle>Código QR</DataTitle>
                    <QrCode src={exampleQr} />
                </QrTitleAndImage>
            </QrContainer>
            <DataTitle>Llave pública</DataTitle>
            <PublicKey>
                <DataBox>
                    <DataText>{walletInfo.address_wallet}</DataText>
                </DataBox>
                <IconButton>
                    <ButtonText>Actualizar</ButtonText>
                </IconButton>
            </PublicKey>
            <ExchangeContainer>
                <NameAndId>
                    <DataBox>
                        <CryptoIcon src={cryptoIcon} />
                        <DataText>t. 02</DataText>
                    </DataBox>
                    <DataBox>                      
                        <ChangeDataText>$. 20</ChangeDataText>
                        <CryptoAmmount>{walletInfo.type_coin}</CryptoAmmount>
                    </DataBox>
                </NameAndId>
                <CryptoChange>
                    <DataTitle>Moneda</DataTitle>
                    <IconButton>
                        <ButtonText>{walletInfo.type_coin}</ButtonText>
                    </IconButton>
                </CryptoChange>
            </ExchangeContainer>
        </Container>
    )
}
