import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 15px;
    -webkit-box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05); 
    box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05);
    margin: 0px 5%; 
    width: 26vw;
    border-radius: 20px;
`

export const Header = styled.div`
    display: flex;
    height: fit-content;
    width: 100%;
    justify-content: space-between;
    margin-bottom: 5%;
`

export const DataTitle = styled.p`
    margin: 10px 0px; 
    font-weight: lighter;
`

export const ButtonsContainer = styled.div`
    display: flex;
    width: 30%;
    justify-content: space-around
`

export const QrContainer = styled.div`
    display: flex;
    justify-content: space-between;
`

export const QrTitleAndImage = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    margin-bottom: 10px;
`

export const QrCode = styled.img`
    height: 70px;
    width: 70px;
`

export const NameAndId = styled.div`
    display: flex;
    flex-direction: column;
    width: 70%;
`

export const DataBox = styled.div`
    padding: 0px 10px;
    box-shadow: 5px 5px 7px 3px rgba(0,0,0,0.05);
    border-radius: 10px;
    margin: 10px 0px;
    display: flex;
`

export const DataText = styled.p`
    color: #99A0AF;
    font-weight: lighter;
    font-size: 12px;
    margin: 10px 5px;
`

export const PublicKey = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
`

export const ButtonText = styled(DataText)`
    margin: 0px;
`

export const ExchangeContainer = styled(QrContainer)`
    
`

export const CryptoChange = styled(QrTitleAndImage)`
    justify-content: center;
`

export const CryptoIcon = styled.img`
    height: 15px;
    align-self: center;
    margin-right: 10px;
`

export const CryptoAmmount = styled(DataText)`
    margin-left: auto;
    margin-right: 15%;
`

export const ChangeDataText = styled(DataText)`
    margin-left: 25px; 
`