import React, {useEffect} from 'react'
import {Container} from './styled'
import EmptyWalletSection from '../EmptyWallet'
import WalletList from '../WalletList'

const WalletView = ({
    longName, 
    icon, 
    name, 
    profileData,
    getWallets,
    walletList,
    deleteWallet,
    deleteSuccess,  
}) => {
    
    useEffect(() => {
        getWallets({
            userId: profileData.id,
            coin: longName,
        })
    }, [deleteSuccess])

    console.log(walletList)
    return(
        <Container>
            {
                walletList && walletList.length > 0 ? 
                    (<WalletList 
                        name={longName}
                        icon={icon} 
                        shortName={name}                    
                        userData={profileData} 
                        walletList={walletList}
                        deleteWallet={deleteWallet}
                    />) 
                : <EmptyWalletSection />
            }
        </Container>
    )
}

export default WalletView