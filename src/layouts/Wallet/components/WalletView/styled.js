import styled from 'styled-components'

export const Container = styled.section`
    display: flex;
    width: 100%;
    justify-content: center;
    background: #F8F8FC;
    margin-bottom: 10%;
`
