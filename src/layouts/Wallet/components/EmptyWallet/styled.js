import styled from 'styled-components'
import {Field} from 'formik'

export const Container = styled.section`
    display: flex;
    width: 100%;
    justify-content: center;
    background: #F8F8FC;
    flex-direction: column;
    align-items: center;
`

export const Message = styled.div`
    color: #3395FF;
    display: flex;
    align-items: center;
`

export const Warning = styled.img`
    margin: 5% 10px;
`

export const WarningMessage = styled.span`

`

export const FormContainer = styled.form`
    max-width: 50vw;
    padding: 3%;
    -webkit-box-shadow: 7px 7px 15px 5px rgba(0,0,0,0.05); 
    box-shadow: 7px 7px 15px 5px rgba(0,0,0,0.05);
    border-radius: 20px;
`


export const FormTitle = styled.p`
    color: #3395FF;
    font-weight: bold;
`

export const FieldContainer = styled.div`
    display: flex;
    margin: 2.5% 10px;
`

export const FieldInput = styled.input`
    display: none;
`

export const FieldLabel = styled.label`
    display: flex;
    width: 100%;
    cursor: pointer;
    -webkit-box-shadow: 7px 7px 15px 2px rgba(0,0,0,0.1); 
    box-shadow: 7px 7px 15px 2px rgba(0,0,0,0.1);
    border-radius: 10px;
    padding-left: 5%;
    padding-right: 2%;
    align-items: center;
    height: 40px
`

export const CustomFileUpload = styled.img`
    height: 20px;
    width: 20px;
    display: flex;
    object-fit: contain;
    margin-left: 20%;
`

export const LabelCheck = styled(CustomFileUpload)`
    margin-left: auto; 
`

export const FormPlaceholder = styled(FormTitle)`
    margin: 0px;
    font-size: 14px;
`

export const ButtonsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin: auto;
    width: 95%;
    margin-top: 30px;
`

export const Button = styled.button`
    background: #ffffff;
    border: none;
    border-radius: 10px;
    padding: 10px 25px;
    font-size: 12px;
    font-weight: bold;
    color: #3395FF;
    width: 20%;
    -webkit-box-shadow: 7px 7px 15px 2px rgba(0,0,0,0.1); 
    box-shadow: 7px 7px 15px 2px rgba(0,0,0,0.1);
    cursor: pointer;


`

export const SubmitButton = styled(Button).attrs({ type: 'submit' })`
    background: #3395FF;
    color: #ffffff;
`