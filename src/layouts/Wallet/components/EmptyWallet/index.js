import React from 'react'
import {
    Container,
    Message,
    Warning, 
    WarningMessage, 
    FormContainer,
    FormTitle,
    FieldContainer,
    FieldInput,
    FieldLabel,
    LabelCheck,
    CustomFileUpload,
    FormPlaceholder,
    ButtonsContainer,
    Button,
    SubmitButton,
} from './styled'
import warning from '../../../../images/icons/warning.svg'
import gallery from '../../../../images/icons/gallery.svg'
import camera from '../../../../images/icons/camera.svg'
import clip from '../../../../images/icons/clip.svg'
import label_check from '../../../../images/icons/label_check.svg'
import {Formik} from 'formik'

const EmptyWalletSection = () => {
    const initialValues = {
        INE: "",
        Selfie: "",
        Comprobante: "",
    };

    function onSubmit(values) {
        console.log(values)
    }

    return(
        <Container>
            <Message>
                <Warning src={warning} />
                <WarningMessage>Aún no tienes monederos</WarningMessage>
                <Warning src={warning} />
            </Message>
            <Formik  validateOnChange={false} {...{ initialValues, onSubmit }}>
                {
                     ({errors, setFieldValue}) => (
                        <FormContainer>
                            <FormTitle>
                                Para gozar de los beneficios de la plataforma, se le solicita completar los siguientes campos:
                            </FormTitle>
                            <FieldContainer>
                                <FieldLabel htmlFor={"INE"}>
                                    <FormPlaceholder>INE</FormPlaceholder>
                                    <LabelCheck src={label_check} />
                                    <CustomFileUpload src={gallery} />
                                </FieldLabel>
                                <FieldInput 
                                    id="INE"
                                    className="INE"
                                    name="INE"
                                    type="file"
                                />
                            </FieldContainer>
                            <FieldContainer>
                                <FieldLabel htmlFor={"Selfie"}>
                                    <FormPlaceholder>Selfie</FormPlaceholder>
                                    <LabelCheck src={label_check} />
                                    <CustomFileUpload src={camera} />
                                </FieldLabel>
                                <FieldInput 
                                    id="Selfie"
                                    className="Selfie"
                                    name="Selfie"
                                    type="file"
                                />
                            </FieldContainer>
                            <FieldContainer>
                                <FieldLabel htmlFor={"Comprobante"}>
                                    <FormPlaceholder>Comprobante de domicilio</FormPlaceholder>
                                    <LabelCheck src={label_check} />
                                    <CustomFileUpload src={clip} />
                                </FieldLabel>
                                <FieldInput 
                                    id="Comprobante"
                                    className="Comprobante"
                                    name="Comprobante"
                                    type="file"
                                />
                            </FieldContainer>
                            <ButtonsContainer>
                                <Button>CANCELAR</Button>
                                <SubmitButton type="submit">ACEPTAR</SubmitButton>
                            </ButtonsContainer>
                        </FormContainer>
                     )
                }
            </Formik>
        </Container>
    )
}

export default EmptyWalletSection