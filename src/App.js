import React from 'react'
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import Layout from './layouts/Router'
import store from './store'
import {OptionsContext, ExchangeMoneyContext} from './Context'

class App extends React.Component {

  changeLanguage(language){
    this.setState({ 
      languageContext: {
        ...this.state.languageContext,
        language: language
      }
     });
  }

  changeExchange(exchange){
    this.setState({ 
      exchangeContext: {
        ...this.state.exchangeContext,
        exchange: exchange
      }
     });
  }

  constructor(props) {
    super(props);
    this.state = {

      languageContext: {
        language: null,
        setLanguage: this.changeLanguage.bind(this)
      },
    
      exchangeContext: {
        exchange: null,
        setExchange: this.changeExchange.bind(this)
      }
    };
  }

  render(){
    return (
      <Provider store={store}>
        <BrowserRouter>
          <OptionsContext.Provider value={this.state.languageContext}>
            <ExchangeMoneyContext.Provider value={this.state.exchangeContext}>
              <Layout />
            </ExchangeMoneyContext.Provider>
          </OptionsContext.Provider>
        </BrowserRouter>
      </Provider>
    )
  }
}

export default App;
