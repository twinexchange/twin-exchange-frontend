import MexicoFlag from '../images/flags/flag_mexico.svg'
import FranceFlag from '../images/flags/flag_france.svg'
import IndiaFlag from '../images/flags/flag_india.svg'
import UsaFlag from '../images/flags/flag_usa.svg'
import ItalyFlag from '../images/flags/flag_italy.svg'
import JapanFlag from '../images/flags/flag_japan.svg'

export const LanguageContext = {
    spanish: {
        name: 'ESP',
        image: MexicoFlag,
    },

    france: {
        name: 'FRA', 
        image: FranceFlag,
    },

    indian: {
        name: 'HUN', 
        image: IndiaFlag,
    },

    english: {
        name: 'ING',
        image: UsaFlag,
    },

    italian: {
        name: 'ITA',
        image: ItalyFlag,
    },

    japanese: {
        name: 'JAP',
        image: JapanFlag,
    }
}

export const ExchangeContext = {
    pesos: {
        name: 'PESOS',
        image: MexicoFlag,
        money: 'MXN'
    },

    francs: {
        name: 'FRANCOS', 
        image: FranceFlag,
        money: 'MXN'
    },

    dollars: {
        name: 'DOLARES', 
        image: IndiaFlag,
        money: 'DLS'
    },

    dollarsUsa: {
        name: 'DOLARES',
        image: UsaFlag,
        money: 'DLS'
    },

    euros: {
        name: 'EUROS',
        image: ItalyFlag,
        money: 'EUR'
    },

    yens: {
        name: 'YENES',
        image: JapanFlag,
        money: 'YEN'
    }
}
