

export const chartGeneralOptions = {
    legend: {
        display: false
    },
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true,
                min: 0,
                max: 0.006    
            },
            gridLines : {
                display : true,
            }
          }],

        xAxes: [{
            ticks: {
                fontColor :"#898F97"
            },
            gridLines : {
                display : false,
            }
        }]
       }
  }