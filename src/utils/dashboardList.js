//cryptos
import addWallet from '../images/icons/add_wallet.svg'
import mexicanPesos from '../images/flags/flag_mexico.svg'
import bitcoin from '../images/icons/crypto/bitcoin.svg'
import litecoin from '../images/icons/crypto/litecoin.svg'
import ethereum from '../images/icons/crypto/ethereum.svg'
import ripple from '../images/icons/crypto/ripple.svg'

//menus
import monedero from '../images/icons/monedero.svg'
import qr from '../images/icons/qr.svg'
import scan from '../images/icons/scan.svg'
import twincoin from '../images/icons/crypto/Twinncoin.svg'
import found from '../images/icons/found.svg'
import change from '../images/icons/change.svg'
import send from '../images/icons/sendArrow.svg'

export const cryptoList = [
    {
        name: "TWC",
        icon: twincoin,
        goTo: "twincoin",
        longName: "Twincoin",
        ammount: "0.000000000",
        borderGraphColor: "#6ED4FD",
        pointBackgroundColor: "#5D93FB",
    },
    {
        name: "MXN",
        icon: mexicanPesos,
        goTo: "pesos",
        longName: "Pesos",
        ammount: "0.000000000",
        //Deactivate Options
        pagosQR: false,
        cobrosQR: false,
        twinpay: false,
        borderGraphColor: "#058E04",
        pointBackgroundColor: "#08A503",
    },
    {
        name: "BTC",
        icon: bitcoin,
        goTo: "bitcoin",
        longName: "Bitcoin",
        ammount: "0.000000000",
        pagosQR: false,
        cobrosQR: false,
        twinpay: false,
        founding: false,
        borderGraphColor: "#FD9626",
        pointBackgroundColor: "#FFC752"
    },
    {
        name: "LTC",
        icon: litecoin,
        goTo: "litecoin",
        longName: "Litecoin",
        ammount: "0.000000000",
        pagosQR: false,
        cobrosQR: false,
        twinpay: false,
        founding: false,
        borderGraphColor: "#0B5CA3",
        pointBackgroundColor: "#0C73B4"
    },
    {
        name: "ETH",
        icon: ethereum,
        goTo: "ethereum",
        longName: "Ethereum",
        ammount: "0.000000000",
        pagosQR: false,
        cobrosQR: false,
        twinpay: false,
        founding: false,
        borderGraphColor: "#777777",
        pointBackgroundColor: "#414141"
    },
    {
        name: "XRP",
        icon: ripple,
        goTo: "ripple",
        longName: "Ripple",
        ammount: "0.000000000",
        pagosQR: false,
        cobrosQR: false,
        twinpay: false,
        founding: false,
        borderGraphColor: "#000000",
        pointBackgroundColor: "#000000"
    },
]

export const actionList = [
    {
        title: "Monederos",
        icon: monedero,
        goTo: "monedero"
    },
    {
        title: "Pagos QR",
        icon: qr,
        goTo: "pagosQR"
    },
    {
        title: "Cobros QR",
        icon: scan,
        goTo: "cobrosQR"
    },
    {
        title: "Twinpay",
        icon: twincoin,
        goTo: "twinpay"
    },
    {
        title: "Fondear",
        icon: found,
        goTo: "founding"
    },
    {
        title: "Canjear",
        icon: change,
        goTo: "change"
    },
    {
        title: "Enviar",
        icon: send,
        goTo: "enviar"
    },
]


export const processActionMenu = (name) => {
    let result = cryptoList.filter((entry) => entry.name === name)

    //It use goTo property

    return [{
        title: "Monederos",
        icon: monedero,
        goTo: "monedero"
    },
    {
        title: "Pagos QR",
        icon: qr,
        goTo: "pagosQR",
        deactivated: result[0].pagosQR === false ? true : false
    },
    {
        title: "Cobros QR",
        icon: scan,
        goTo: "cobrosQR",
        deactivated: result[0].cobrosQR === false ? true : false
    },
    {
        title: "Twinpay",
        icon: twincoin,
        goTo: "twinpay",
        deactivated: result[0].twinpay === false ? true : false
    },
    {
        title: "Fondear",
        icon: found,
        goTo: "founding",
        deactivated: result[0].founding === false ? true : false
    },
    {
        title: "Canjear",
        icon: change,
        goTo: "change"
    },
    {
        title: "Enviar",
        icon: send,
        goTo: "enviar"
    }]
}