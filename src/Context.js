import React from 'react'

const OptionsContext = React.createContext({
    language: null,
    setLanguage: () => {},
})

const ExchangeMoneyContext = React.createContext({
    exchange: null,
    setExchange: () => {}
})


export { OptionsContext, ExchangeMoneyContext };